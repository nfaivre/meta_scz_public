function [final_vec]= randConstr_(ntrials,condsVec,repmax)
% ntrials is the desired number of trials in each condition specified in the condsVec

if nargin<3; error('You must enter 3 arguments: vec = randConstrNathan(ntrials,condsVec,repmax)'); end

vec_nonRand         = sort(repmat(1:length(condsVec),1,ntrials));    % order of vec conditions
vec_rand            = zeros(1,length(vec_nonRand));
final_vec           = zeros(size(vec_rand));

init_time=tic;

for t = 1:length(vec_rand)
    randPosition    = randsample(length(vec_nonRand),1);
    vec_rand(t)     = vec_nonRand(randPosition);
    % Work around: when getting to last 3 elements to place in vec_rand,
    % check whether there remains only same elements in vec_nonRand, if yes
    % restart randomization by calling the present function
    if t==length(vec_rand)-repmax+1 && range(vec_nonRand)==0
        final_vec = randConstrNathan(ntrials,condsVec,repmax);
        return;
    end
    while t >= repmax && ~range(vec_rand(t-(repmax-1): t)) && toc(init_time)<3 % give a 3s timeout in case we're stuck
        randPosition    = randsample(length(vec_nonRand),1);
        vec_rand(t)     = vec_nonRand(randPosition);  
    end
    vec_nonRand(randPosition) = [];
end
vec = vec_rand;

% Replace unitary values by the conditions' values
for ii=1:length(condsVec)
    final_vec(ismember(vec,ii))=condsVec(ii);
%     inds_cond=find(vec(:)==ii);
%     final_vec(inds_cond)=condsVec(ii);
end
