function [xyDots, dirDots] = rdm_stim2(nbFrames,nbDots,lifetime,stimRadPix,m,s,dir_trim,speedPPF,tolm,tols)
% function xyDots = rdmstim(nbFrames,nbDots,lifetime,stimRadPix,m,s,dir_trim)
%
% this function is useful to generate a random dot motion stimulus
% it produces the coordinates of the dots on successive frames
% note: the function does not display anything! use your own tools for
% display
%
% version2: dots' directions is re-generated at each frame, even during the
% lifetime
%
%
% input:
% nbFrames = number of frames
% nbDots = number of dots
% lifetime = number of frames during which a dot has a continuous motion
% stimRadPix = circular extension of the stimulus (radius in pixels)
% m = mean of the gaussian distribution for motion directions (in rad)
% s = sigma of the gaussian distribution for motion directions (in rad)
% dir_trim = number of sigma for trimming the motion directions
% speedPPF
% tolm = tolerance for m (in degrees)
% tols = tolerance for s (in degrees)
% output:
% xyDots = nbDots x 2 x nbFrames = x and y positions (in pixels)
% dirDots = nbDots x nbFrames = directions (in rad)

if nargin<9
    tolm=1;
end
if nargin<10
    tols=1;
end
if s<0
    s=0;
    warning('rdm_stim2 called with negative variance... bad idea, using zero variance instead'); %#ok<WNTAG>
end

dotList = 1:nbDots;
xyDots = nan(nbDots, 2, nbFrames);
dirDots = nan(nbDots, nbFrames);

check = false;
ncheck=0;
while not(check)

    for ff=1:nbFrames,

        % select dots to refresh
        if ff==1
            dot_refresh = true(1,nbDots); % at start, all dots are resampled
        else
            xyDots(:, :, ff) = xyDots(:, :, ff-1); % by default previous position will be the starting point
            dot_refresh = mod(dotList + ff, lifetime)==1; % except some that will be resampled
        end
        n_refresh = sum(dot_refresh);

        % new positions for some dots
        not_ok = true(1,n_refresh); % some dots are re-sampled
        xy_refresh = nan(2,n_refresh);
        while any(not_ok)
            xy_refresh(1:2, not_ok) = (rand(2,sum(not_ok))*2 - 1) * stimRadPix;
            rr = sqrt(sum(xy_refresh.^2));
            not_ok(rr<stimRadPix) = false;
        end
        xyDots(dot_refresh, :, ff) = xy_refresh';
        if ff>1
            xyDots(not(dot_refresh), :, ff) = xyDots(not(dot_refresh), :, ff-1) ;
        end

        % new directions for all dots
        not_ok = true(1,nbDots);
        dir_refresh = nan(1,nbDots);
        if s>0
            while any(not_ok)
                dir_refresh(not_ok) = randcircvm(m,1/s^2,sum(not_ok)); % von Mises distribution
                not_ok(abs(distangle(dir_refresh,m))< dir_trim*s) = false; % angular distance
            end
        else
            dir_refresh = ones(1,nbDots)*m;
        end
        dirDots(:,ff) = dir_refresh;

        % update dots that are not relocated
        dot_update = not(dot_refresh);
        if any(dot_update)
            xyDots(dot_update, 1, ff) = xyDots(dot_update, 1, ff-1) + cos(dirDots(dot_update,ff-1)) * speedPPF;
            xyDots(dot_update, 2, ff) = xyDots(dot_update, 2, ff-1) - sin(dirDots(dot_update,ff-1)) * speedPPF;
            % reposition on opposite side of circle if dots go to far
            rr = sqrt(sum(xyDots(:,:,ff).^2,2));
            repos = rr >= stimRadPix;
            if any(repos)
                xyDots(repos, :, ff) = - xyDots(repos, :, ff-1);
            end
        end
    end

    if s>0
        truemean = circmean(dirDots(:));
        truedevi = 1./sqrt(circkappa(dirDots(:)));
        checkmean = abs(distangle(truemean,m)) < tolm * pi/180; % criterion for the actual mean = 1� away from the desired mean
        checkdevi = abs(truedevi-s) < tols * pi/180; % criterion for the actual mean = 1� away from the desired mean
        check = checkmean & checkdevi;
    else
        check=1;
    end
    ncheck = ncheck+1;
end
% disp(ncheck)
end


function alpha = randcircvm(theta, kappa, n)
% alpha = circ_vmrnd(theta, kappa, n)
%   Simulates n random angles from a von Mises distribution, with preferred
%   direction thetahat and concentration parameter kappa.
%   Input:
%     [theta    preferred direction, default is 0]
%     [kappa    width, default is 1]
%     [n        number of samples, defailt is 10]
%   Output:
%     alpha     samples from von Mises distribution
%   References:
%     Statistical analysis of circular data, Fisher, sec. 3.3.6, p. 49
% Circular Statistics Toolbox for Matlab
% By Philipp Berens and Marc J. Velasco, 2009
% velasco@ccs.fau.edu
% default parameter
if nargin < 3
    n = 10;
end
if nargin < 2
    kappa = 1;
end
if nargin < 1
    theta = 0;
end
% if kappa is small, treat as uniform distribution
if kappa < 1e-6
    alpha = 2*pi*rand(n,1);
    return
end
% other cases
a = 1 + sqrt((1+4*kappa.^2));
b = (a - sqrt(2*a))/(2*kappa);
r = (1 + b^2)/(2*b);
alpha = zeros(n,1);
for j = 1:n
    while true
        u = rand(3,1);
        z = cos(pi*u(1));
        f = (1+r*z)/(r+z);
        c = kappa*(r-f);
        if u(2) < c * (2-c) || ~(log(c)-log(u(2)) + 1 -c < 0);
            break
        end
    end
    alpha(j) = theta +  sign(u(3) - 0.5) * acos(f);
end
alpha = angle(exp(1i*alpha));
end


function m = circmean(d)
m = angle(mean(exp(1i*d)));
end

function d = distangle(a,b)
d = angle(exp(1i*a)./exp(1i*b));
end

function kappa = circkappa(a)
% compute the kappa parameter for Von Mises distribution (from CircStats toolbox)
R = abs(mean(exp(1i*a(:))));
if R < 0.53
    kappa = 2*R + R^3 + 5*R^5/6;
elseif R>=0.53 && R<0.85
    kappa = -.4 + 1.39*R + 0.43/(1-R);
else
    kappa = 1/(R^3 - 4*R^2 + 3*R);
end
end
