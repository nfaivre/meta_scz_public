% draw the VAS in PTB
function drawVAS_ptb_TOJ(VASrange)
    
    global win 
    global midX midY  
    
    lineWidth = 2;
    yup       = 10;
    VASlength = range(VASrange);
    
    
    stringTop  = 'Sur d''avoir bon';
    stringBottom = 'Sur d''avoir faux';
    stringMiddle = 'Reponse au hasard';
    
    vertLines_y = midY + VASlength/2 * [-1 -1 -.75 -.75 -.5 -.5 -.25 -.25 0 0 .25 .25 .5 .5 .75 .75 1 1];
    vertLines_x = midX + yup * repmat([1 -1], 1, length(unique(vertLines_y)));
    middleVerticals = find(vertLines_y == midY);
    vertLines_x(middleVerticals(1)) = midX + yup * 2;
    vertLines_x(middleVerticals(2)) = midX + yup * -2;                    %make middle line a bit longer
    horizLine_y = VASrange;
    horizLine_x = midX + [0          0];                                  %this is a little bit ridiculous but will depend on the size of the * used as a cursor
    
%     Screen('TextSize', win, 16);                                    
    Screen('DrawLines', win, [vertLines_x; vertLines_y], lineWidth);
    Screen('DrawLines', win, [horizLine_x; horizLine_y], lineWidth);
%     DrawFormattedText(win, 'How confident?', midX - 60,'center');
    DrawFormattedText(win, stringBottom, midX + 30,VASrange(2),[230 20 0]);
    DrawFormattedText(win, stringTop,  midX + 30,VASrange(1) ,[20 230 0]);
	DrawFormattedText(win, stringMiddle,  midX + 30,midY +10,[0 0 0]);

end

