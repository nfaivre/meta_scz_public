function P = vertical_vas(P,b, t, screenRect,trigQ2,trigM2,trigR2,s,dosendtriggers,CrossWidth)

global win
global midY midX

VASrange    = [midY-150, midY+150];

% starty = VASrange(1) + (VASrange(2) - VASrange(1)) * rand; % for random start
starty = (VASrange(1) + VASrange(2)) /2; % for start in the middle
initialy = round(starty);
P.VAS.mousePosOrig(t) = initialy ; %(initialy - VASrange(1))/range(VASrange);
SetMouse(0,initialy,win);


%Intialize
P.VAS.onset(b,t) = GetSecs;
counter = 1; mousetrail_y = [];
movementBegun   = 0;
mouseButtons    = 0;

tic;
% trigger
% if dosendtriggers==1,outp(Address,trigQ2); WaitSecs(0.005);outp(Address, 0);end
if dosendtriggers,outputSingleScan(s,dec2binvec(trigQ2,8)); WaitSecs(0.005);outputSingleScan(s, dec2binvec(0,8));end

while 1
    % draw scale
    [mouseX, mouseY, mouseButtons] = GetMouse;
    draw_vertical_vas(VASrange);
    
    % set spatial limits to the scale
    if mouseY > VASrange(2)
        mouseY = VASrange(2);
        SetMouse(mouseX,mouseY,win);
        
    elseif mouseY < VASrange(1)
        mouseY = VASrange(1);
        SetMouse(mouseX,mouseY,win);
    end
    
    % draw cursor
%         DrawFormattedText(win, '+', midX-(CrossWidth(3)/2),mouseY -(CrossWidth(4)/2),0);
    Screen('DrawText',win, '+', midX-(CrossWidth(3)/2)-1,mouseY -(CrossWidth(4)/2),0);
    
    %     conftmp=(VASrange(2) - mouseY)/range(VASrange);
    conftmp=(VASrange(2) - mouseY)/(max(VASrange)-min(VASrange));
    
    %     DrawFormattedText(win, sprintf('%d%%',round(conftmp*100)), midX +40,midY-10,0);
    Screen('DrawText',win,sprintf('%d%%',round(conftmp*100)), midX -120,midY-10,0);
    
    Screen('Flip',win); % display
    
    % detects the first movement made by the mouse
    if mouseY ~= initialy && ~movementBegun
        if dosendtriggers,outputSingleScan(s,dec2binvec(trigM2,8)); WaitSecs(0.005);outputSingleScan(s, dec2binvec(0,8));end
        P.VAS.firstRT(b,t) = GetSecs - P.VAS.onset(b,t);
        movementBegun = 1;
    end
    
    mousetrail_y(counter)   = mouseY; % save Y trajectory
    moveTime(counter)       = GetSecs - P.VAS.onset(b,t);
    counter                 = counter + 1;
    
    %% detect clicks
    if mouseButtons(1)>0 % left click
        % trigger
%         if dosendtriggers==1,outp(Address,trigR2); WaitSecs(0.005);outp(Address, 0);end
        if dosendtriggers,outputSingleScan(s,dec2binvec(trigR2,8)); WaitSecs(0.005);outputSingleScan(s, dec2binvec(0,8));end

        P.VAS.confidence(b,t)        = conftmp;
        P.VAS.conf_absolute(b,t)     = mouseY;
        P.VAS.movRT(b,t)             = GetSecs - P.VAS.onset(b,t);
        break
        
    elseif any(mouseButtons(2:length(mouseButtons))>0) % cancel trial if right click
        % trigger
%         if dosendtriggers==1,outp(Address,trigR2); WaitSecs(0.005);outp(Address, 0);end
%         P.VAS.confidence(b,t)        = 999;
%         P.VAS.conf_absolute(b,t)     = mouseY;
%         P.VAS.movRT(b,t)             = GetSecs - P.VAS.onset(b,t);
        break
    end
    
    
end
P.VAS.Y(b,t).Y = mousetrail_y;

if ~movementBegun
    P.VAS.firstRT(b,t)      = NaN;
end

% give feedback only for training
if P.feedback
    if P.cor(b,t)==1
        feedbackColor = 255 * [0 1 0];  %green
    else
        feedbackColor = 255 * [1 0 0];  %red
    end
    
    draw_vertical_vas(VASrange); % redraw a normal scale
    %         DrawFormattedText(win, '+', midX -(CrossWidth(3)/2), mouseY-(CrossWidth(4)/2), feedbackColor); % add color feedback
    Screen('DrawText',win, '+', midX -(CrossWidth(3)/2), mouseY-(CrossWidth(4)/2), feedbackColor); % add color feedback
    
    Screen('Flip',win);
    WaitSecs(.5);
    Screen('TextColor',win, 0); %set text color back to black
end

draw_vertical_vas(VASrange);
Screen('Flip',win);
WaitSecs(.5);


Screen('Flip',win);
end
