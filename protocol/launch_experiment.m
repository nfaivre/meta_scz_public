%% README
% Experiment in which a 2afc on RDK direction is followed by confidence judgments on VAS
% RDK performance is set by 1up/2down procedure, responses are provided with the mouse
% Sends triggers to Brainamp system

function launch_experiment(stairtrain)
close all
warning off MATLAB:m_warning_end_without_block
clc
commandwindow; 

dosendtriggers = 0; % defines if eeg triggers are sent

if stairtrain>3
    error('bad input')
end

addpath([pwd filesep 'functions']); % add the subfolder with functions we need

%% decide if we send trigger to Brain amp system (EEG)
if dosendtriggers
    s = daq.createSession('ni'); % setup the session
    ch = addDigitalChannel(s,'Dev1', 'Port2/Line0:7', 'OutputOnly'); % Setup an 8­bit range as a channel to which events can be written
else s = 0; ch=0;
end

global win
global midY midX

%% GET PARTICIPANT'S INFOS
dlg = inputdlg({'Subject #','Age','Sexe (h/f)'},'Input');
P.suj.number            = str2num(dlg{1});
P.suj.age               = dlg{2};
P.suj.sex               = dlg{3};
P.stairtrain            = stairtrain; % decide if you run a first training staircase (1)
P.feedback              = 0; % decide if we do feedback or real expe
P.suj.startdate         = clock; % save the date & time

% attribute std values if missing
if isempty(P.suj.age),P.suj.age=99;end
if isempty(P.suj.number),P.suj.number=99;end
if isempty(P.suj.sex),P.suj.sex='x';end

%% Staircase
if P.stairtrain == 0; % initial training session
    sigrefs = repmat(30 * pi/180,1,1);
elseif P.stairtrain == 1; % staircase session
    sigrefs = repmat(40 * pi/180,1,1); % reference of sd
elseif P.stairtrain == 2; % VAS training session
    sigrefs = repmat(30 * pi/180,1,1);
elseif P.stairtrain == 3; % real session
    stairfile=[pwd filesep 'data' filesep 'data_' num2str(P.suj.number) '_' P.suj.sex '_' num2str(P.suj.age) '_1_' datestr(datestr(now),1) '.mat']; % loads results from staircase session
    
    exist(stairfile)
    if exist(stairfile)>0; % test if file exists
        staircasedat = load(stairfile);
        tmp=[staircasedat.P.signal];
        sigrefs = [mean(tmp(1,end-5:end))];
        fprintf('OK. Anciens seuils trouves: %.2f.\n',sigrefs); WaitSecs(1);
    else
        while 1
            sigref=input('Je ne trouve pas l''ancien seuil, merci d''appeler l''experimentateur: ','s');
            sigref=str2num(sigref);
            if Sigrid < pi & sigref > 0.5
                sigrefs = sigref;
                fprintf('OK. nouveaux seuils definis: %.2f.\n', sigref); WaitSecs(1);
                break
            else fprintf('Indiquez un seuil en radians entre 0.5 et 3.14.\n')
            end
        end
    end
end

P.stepsize = 2* pi/180;
P.Staircase = SetupStaircase(1, sigrefs, [pi/180 pi], [2 1])

%% PREPARE STIMS & DESIGN
screen.id                  = 0; % main screen (consider changing if we have several screens)
screen.colbackground       = 128; % color of the background
screen.refreshrate         = Screen('FrameRate', screen.id); % in Hz
screen.refreshtime         = 1000/screen.refreshrate; % in ms
[win, screen.rect]  = Screen('OpenWindow', screen.id, screen.colbackground, [], 32,2);

screen.center = [screen.rect(3)/2 screen.rect(4)/2]; % Screen center position (Mac screen coordinates)
midX=screen.center(1); midY=screen.center(2);

screen.ppi              = get(0,'ScreenPixelsPerInch');
screen.size             = get(0,'ScreenSize');
screen.dotPitch         = 25.4 ./ screen.ppi; % mm per pixels
screen.viewingDistance  = 573; % viewing distance in mm
screen.pixTOdeg         = atan(screen.dotPitch/screen.viewingDistance)*180/pi; % conversion pixels to degrees
screen.degTOpix         = 1/screen.pixTOdeg;  % conversion degrees (vis. angle) to pixels
screen.res              = Screen('Resolution',win);
screen.framedur         = Screen('GetFlipInterval', win); % sec / frame
Screen('TextSize', win, 28);

%% stimuli & design
switch P.stairtrain
    case 0
        P.design.blocknumber    = 1; % block of trials
        P.design.ntrials        = 10; % number of trials for training
    case 1
        P.design.blocknumber    = 1; % block of trials
        P.design.ntrials        = 100; % number of trials for initial staircase
    case 2
        P.design.blocknumber    = 1; % block of trials
        P.design.ntrials        = 10; % number of trials for initial staircase
    case 3
        P.design.blocknumber    = 1:10; % block of trials numbered from 1 to 10
        P.design.ntrials        = 30; % number of trials for real expe
end

% create alternations of left/right movements for each block
for b = P.design.blocknumber
    P.design.stims(b,:) = randConstr_(P.design.ntrials/2,[1 2],4);
end

P.design.stimsize       = screen.rect(4)/10;
P.design.stimdur        = 1; % stimulus duration in seconds
P.design.fixdur         = .25; % fixation duration in seconds
P.design.RTmax          = 6; % maximal reaction time beyond which we play audiofeedback
Screen('Preference', 'DefaultFontSize',28)
Screen('TextSize', win,28);
tmp = Screen('TextBounds', win, 'START',midX,midY);
P.design.initclick([1,3]) = (tmp([1,3])+midX) - ((tmp(3)-tmp(1))/2);
P.design.initclick([2,4]) = (tmp([2,4])+midY) - ((tmp(4)-tmp(2))/2);

P.design.resprectL = [midX-2*P.design.stimsize, (midY-P.design.stimsize/2)-2*P.design.stimsize, ...
    midX-P.design.stimsize, (midY+P.design.stimsize/2)-2*P.design.stimsize]; % Coordinates of the Left response circle

P.design.resprectR = [midX+P.design.stimsize, (midY-P.design.stimsize/2)-2*P.design.stimsize, ...
    midX+2*P.design.stimsize, (midY+P.design.stimsize/2)-2*P.design.stimsize]; % Coordinates of the Right response circle

R = (P.design.resprectL(3)-P.design.resprectL(1))/2; % radius of response circle
P.design.resprectLmid = [P.design.resprectL(3)-R, P.design.resprectL(4)-R]; % middle of response circle (L)
P.design.resprectRmid = [P.design.resprectR(3)-R,P.design.resprectR(4)-R]; % middle of response circle (R)

%% P.rdk: contains all the parameters for RDK
P.rdk.nbDots                = 100;  % RDM stimulus
P.rdk.lifetime              = 10;   % RDM stimulus % 60Hz
P.rdk.fixationDuration      = .300; % fixation duration (sec)
P.rdk.StimulusOnDuration    = 10;     % 'targets on' duration (sec)
P.rdk.rampDuration          = 10;   % contrast ramp time in frame (at begining and end of the stimulus)
P.rdk.dir_trim              = 3;    % nb of sd to trim dot directions
P.rdk.speedDPS              = 3.0;  % speed (deg / sec)
P.rdk.stimRadDeg            = 3.0;  % radius of stimulus (deg)
P.rdk.critRadDeg            = 3.0;  % radius of criterion frame (deg)
P.rdk.radDiscDeg            = 0.1;  % radius of dots (deg)
P.rdk.minCritFrame          = 0.0;  % angle between criterion and start of frame (deg)
P.rdk.sizeCritFrame         = 180;  % angle between start and end of criterion frame (deg)
P.rdk.frameThick            = 5;   % frame thickness (pixels)
P.rdk.lineHalfLenDeg        = 0.5;
P.rdk.fixRadDeg             = 0.1;  % radius of fixation point (deg)
P.rdk.fixClrDeg             = 0.5;  % radius of cleared zone around fixation point (deg)
P.rdk.contrast              = 0.8;
P.rdk.audiofeedback         = true;
whitedotsfull               = screen.colbackground + (255 - screen.colbackground) .* P.rdk.contrast;
frame_blue                  = [120, 120, 237]; % color of blue frame
frame_red                   = [182, 113, 120]; % color of red frame

% timing parameters
nbFrames = ceil(P.rdk.StimulusOnDuration / screen.framedur); % 24 frames = 400 ms @ 60Hz
waitframe = .5 * screen.framedur;
trials.dirDots1 = repmat(NaN,[P.design.ntrials,P.rdk.nbDots,nbFrames]);
trials.xyDots1 = repmat(NaN,[P.design.ntrials,P.rdk.nbDots,2,nbFrames]);

% ramp in contrast
whitedots = whitedotsfull*ones(1,nbFrames);
ramping = linspace(screen.colbackground,whitedotsfull,P.rdk.rampDuration+2);
whitedots(1:P.rdk.rampDuration) = ramping(2:(end-1));
whitedots((nbFrames-P.rdk.rampDuration+1):nbFrames) = ramping((end-1):-1:2);

% deg to pix conversion
P.rdk.speedPPF = P.rdk.speedDPS * screen.degTOpix * screen.framedur; % speed (pixels / frame)
P.rdk.stimRadPix = P.rdk.stimRadDeg * screen.degTOpix; % radius of stimulus (pixels)
P.rdk.critRadPix = P.rdk.critRadDeg * screen.degTOpix; % radius of crterion frame (pixels)
P.rdk.radDiscPix = P.rdk.radDiscDeg * screen.degTOpix; % radius of dots (pixels)
P.rdk.lineHalfLenPix = P.rdk.lineHalfLenDeg * screen.degTOpix;
P.rdk.fixRadPix = P.rdk.fixRadDeg * screen.degTOpix; % radius of fixation point (pixels)
P.rdk.fixClrPix = P.rdk.fixClrDeg * screen.degTOpix;

% vertical lines for reference
crit_deg = 90;
crit = 90 * pi/180;
criterionLines  = (P.rdk.critRadPix*[cos(crit);-sin(crit)]*[1 1]) + (P.rdk.lineHalfLenPix*[cos(crit);-sin(crit)]*[1 3]);
criterionLines2 = [criterionLines, -criterionLines];

%% Beeping sound
% InitializePsychSound; % Initialize Sounddriver
nrchannels      = 1; % Number of channels
freq            = 44100; % Frequency of the sound
beepLengthSecs  = .75; % Length of the beep
mybuzz          = .75.*mysawtooth(2*pi*200*(0:beepLengthSecs*freq)/freq);


%% Configure keyboard
KbName('UnifyKeyNames');
P.quitkey = KbName('ESCAPE');
P.spaceKey = KbName('SPACE');

HideCursor();
CrossWidth   = Screen('TextBounds', win, '+');

P.rdk.frameHalfSize = P.rdk.critRadPix + P.rdk.lineHalfLenPix + P.rdk.frameThick;
criterionRect = [-P.rdk.frameHalfSize + screen.center(1), -P.rdk.frameHalfSize + screen.center(2), ...
    P.rdk.frameHalfSize + screen.center(1), P.rdk.frameHalfSize + screen.center(2)];
crit_deg = 90;
crit = 90 * pi/180;
criterionLines = (P.rdk.critRadPix*[cos(crit);-sin(crit)]*[1 1]) + (P.rdk.lineHalfLenPix*[cos(crit);-sin(crit)]*[1 3]);
criterionLiner = (P.rdk.critRadPix*[cos(crit+pi/2);-sin(crit+pi/2)]*[1 1]) + (P.rdk.lineHalfLenPix*[cos(crit+pi/2);-sin(crit+pi/2)]*[1 3]);
criterionLines = [+criterionLines, -criterionLines];

%% define triggers
trigstart   = 101; % trial onset: goes from 101 to 101 + P.design.trialperblock (101:180)
trigstim    = 12; % stim onset
trigM1      = 13; % M1 mouse first move
trigR1      = 14; % R1 provided
trigQ2      = 15; % Q2 onset
trigM2      = 16; % M2 mouse first move during VAS
trigR2      = 17; % R2 provided

%% START LOOPING THROUGH BLOCKS
for b = P.design.blocknumber;
    Screen('TextSize', win, 28);
    
    [keyIsDown, Secs, keyCode] = KbCheck;    if keyCode(P.quitkey), break, end
    
    blocktype = 'unique';
   
    %% welcome screen
    if P.stairtrain == 3;
        DrawFormattedText(win, ['Bloc ' num2str(b) ' sur ' num2str(max(P.design.blocknumber))]  , 'center', 'center', [255, 255, 255, 255]);
    end
    
    Screen('FrameArc', win, frame_blue, criterionRect,  90-crit_deg-P.rdk.minCritFrame, -P.rdk.sizeCritFrame, P.rdk.frameThick, P.rdk.frameThick);
    Screen('FrameArc', win, frame_red,  criterionRect,  90-crit_deg+P.rdk.minCritFrame,  P.rdk.sizeCritFrame, P.rdk.frameThick, P.rdk.frameThick);
    Screen('DrawLines', win, criterionLines, P.rdk.fixRadPix, 0, screen.center, 0);
    
    if P.stairtrain == 1 | P.stairtrain == 3;
       
        DrawFormattedText( win, 'Vous avez 6 secondes pour regarder les points',  'center', 200, [255, 255, 255, 255]);
       
    elseif P.stairtrain ==0 | P.stairtrain == 2;
        
	DrawFormattedText(win, 'Cette partie est un entrainement pour vous familiariser avec l''experience',  'center', 200, [255, 255, 255, 255]);

    end
    
    DrawFormattedText(win, 'Appuyez sur une touche pour demarrer',  'center', 260, [255, 255, 255, 255]);
    Screen('Flip', win);
    [secs, keyCode]=KbStrokeWait;
    
    
    %% START LOOPING THROUGH TRIALS
    for t=1:P.design.ntrials
        Screen('TextSize', win, 28);
        
        % read the keyboard to quit if needed
        [keyIsDown, Secs, keyCode] = KbCheck;
        if keyCode(P.quitkey);
            commandwindow;
            sca;
        end
        
        P.blocktype{b,t} = blocktype;
        
        % sigref for VAS training
        if P.stairtrain == 2;
            if t<=P.design.ntrials/2;
                P.rdk.sigref(b,t) = 30*pi/180;
            elseif t>P.design.ntrials/2;
                P.rdk.sigref(b,t) = 150*pi/180;
            end
        end
        
        X=[];Y=[]; % mouse place holders
        
        % determine angle of RDK
        switch P.design.stims(b,t);
            case 1
                P.rdk.m(b,t) = 135*pi/180; % left
            case 2
                P.rdk.m(b,t) = 45*pi/180; % right
        end
        
        % determine angle of RDK based on staircase
        if  P.stairtrain ~=2
            
            P.rdk.sigref(b,t) = P.Staircase(1).Signal; % coh
            
        end
        
        
        %% For Real Session, start each trial by clicking on central box (to reposition the mouse)
        if P.stairtrain ==3 | P.stairtrain == 1;
            SetMouse(midX, P.design.resprectL(2)); % set the cursor high on screen to make participants lower the mouse
            while 1
                % draw start box
                [nx, ny, startbounds]=DrawFormattedText(win, 'START','center', midY, 255);
                Screen('FrameRect', win, [255], [startbounds(1)-20,startbounds(2)-20, startbounds(3)+20, startbounds(4)+20], P.rdk.frameThick/5);
                
                %  ShowCursor;
                [x,y,buttons] = GetMouse();
                Screen('DrawText', win, '+',  x, y, 0);
                
                % start rule
                if any(buttons) & x <(startbounds(3)+20) & x>(startbounds(1)-20)...
                        & y<(startbounds(4)+20) & y>(startbounds(2)-40)
                    break
                end
                Screen('Flip', win);
            end
        end
        if dosendtriggers,outputSingleScan(s,dec2binvec(mod(t-1,80)+trigstart,8)); WaitSecs(0.005);outputSingleScan(s, dec2binvec(0,8));end
        
        HideCursor;
        [xyDots1,dirDots1] = rdm_stim2(nbFrames,P.rdk.nbDots,P.rdk.lifetime,P.rdk.stimRadPix,P.rdk.m(b,t),P.rdk.sigref(b,t),P.rdk.dir_trim,P.rdk.speedPPF,1,3);
        Screen('Flip', win);
        %% fixation cross
        Screen('FrameArc', win, frame_blue, criterionRect,  90-crit_deg-P.rdk.minCritFrame, -P.rdk.sizeCritFrame, P.rdk.frameThick, P.rdk.frameThick);
        Screen('FrameArc', win, frame_red,  criterionRect,  90-crit_deg+P.rdk.minCritFrame,  P.rdk.sizeCritFrame, P.rdk.frameThick, P.rdk.frameThick);
        Screen('FrameOval', win, frame_blue, P.design.resprectL,P.rdk.frameThick);
        Screen('FrameOval', win, frame_red , P.design.resprectR,P.rdk.frameThick);
        Screen('DrawDots', win, [0;0], P.rdk.fixRadPix, 0, screen.center, 1);
        Screen('Flip', win);
        WaitSecs(P.design.fixdur);
        
        % Cursor initial position
        SetMouse(round(midX-(CrossWidth(3)/2)),midY-(CrossWidth(4))/2,screen.id);
        
        % RDM stimulus 1
        dur(1)=P.rdk.fixationDuration;
        dur(2:nbFrames)=screen.framedur;
        
        % EEG trigger

        if dosendtriggers,outputSingleScan(s,dec2binvec(trigstim,8)); WaitSecs(0.005);outputSingleScan(s, dec2binvec(0,8));end
        vbl = GetSecs; % timeref
        P.stimonset(b,t) = vbl;
        mousemove = 0;
        for ff=1:nbFrames
            [x,y,buttons] = GetMouse();
            
            X=[X x];
            Y=[Y y];
            P.Xs(b,t).X=X; %  creates a substructure for each trial in which we put the x coordinates (allows different sizes of X on each trial)
            P.Ys(b,t).Y=Y;
            
            % EEG trigger
            if ff>1
                if X(ff-1) ~= X(ff) && mousemove==0 && dosendtriggers
                    outputSingleScan(s,dec2binvec(trigM1,8)); WaitSecs(0.005);outputSingleScan(s, dec2binvec(0,8));
                    mousemove =1;
                end
            end
            
            % Set boundaries to the mouse
            if y < P.design.resprectR(2) +9;
                y = P.design.resprectR(2) +9;
                SetMouse(round(x),round(y), screen.id);end
            if x < P.design.resprectL(1) +24;
                x = P.design.resprectL(1) +24;
                SetMouse(round(x),round(y), screen.id);end
            if x > P.design.resprectR(3) -30;
                x = P.design.resprectR(3) -30;
                SetMouse(round(x),round(y), screen.id);end
            if y > midY;
                y = midY;
                SetMouse(round(x),round(y), screen.id);end
            
            % draw stimulus
            Screen('FrameArc', win, frame_blue, criterionRect,  90-crit_deg-P.rdk.minCritFrame, -P.rdk.sizeCritFrame, P.rdk.frameThick, P.rdk.frameThick);
            Screen('FrameArc', win, frame_red,  criterionRect,  90-crit_deg+P.rdk.minCritFrame,  P.rdk.sizeCritFrame, P.rdk.frameThick, P.rdk.frameThick);        % Screen('FillRect',win, 128);
            Screen('DrawDots', win, xyDots1(:,:,ff)', P.rdk.radDiscPix, whitedots(ff), screen.center, 1);
            
            % draw response circles
            Screen('FrameOval', win, frame_blue, P.design.resprectL,P.rdk.frameThick);
            Screen('FrameOval', win, frame_red , P.design.resprectR,P.rdk.frameThick);
            
            % draw cross & fix
            Screen('DrawText', win, '+',  x, y, 0);
            Screen('DrawDots', win, [0;0], P.rdk.fixRadPix, 0, screen.center, 1);
            vbl = Screen('Flip', win, vbl + dur(ff) - waitframe);
            timing.stim1(b,t,ff) = vbl;
            
            
            
            % Response Rules
            if any(buttons) && (((x-P.design.resprectLmid(1))^2) + ((y-P.design.resprectLmid(2))^2)) <= R^2;

                if dosendtriggers,outputSingleScan(s,dec2binvec(trigR1,8)); WaitSecs(0.005);outputSingleScan(s, dec2binvec(0,8));end
                P.tclick(b,t) = GetSecs;
                P.clickside(b,t) = 1;
                break
            elseif any(buttons) && (((x-P.design.resprectRmid(1))^2) + ((y-P.design.resprectRmid(2))^2)) <= R^2;

                if dosendtriggers,outputSingleScan(s,dec2binvec(trigR1,8)); WaitSecs(0.005);outputSingleScan(s, dec2binvec(0,8));end
                
                P.tclick(b,t) = GetSecs;
                P.clickside(b,t) = 2;
                break
            elseif ff==nbFrames;

                if dosendtriggers,outputSingleScan(s,dec2binvec(trigR1,8)); WaitSecs(0.005);outputSingleScan(s, dec2binvec(0,8));end
                
                P.tclick(b,t) = GetSecs;
                P.clickside(b,t) = 99;
            end
        end
        
        %% compute accuracy & RTs
        P.RT(b,t) =  P.tclick(b,t)- P.stimonset(b,t); % reaction timesRT
        
        % sound for 'fast' blocks in 'stairtrain' and real experiment sessions
        if P.stairtrain == 1 | P.stairtrain ==3;

            if P.RT(b,t) > P.design.RTmax;
                sound(mybuzz,freq);
                WaitSecs(beepLengthSecs);
            end
            if P.design.stims(b,t)==P.clickside(b,t);
                P.cor(b,t) = 1;
            elseif P.design.stims(b,t)~=P.clickside(b,t) && P.clickside(b,t)<99;
                P.cor(b,t) = 0;
            elseif P.clickside(b,t)==99
                P.cor(b,t) = 99;
            end
        elseif P.stairtrain ==0;
            if P.design.stims(b,t)==P.clickside(b,t);
                P.cor(b,t) = 1;
            elseif P.design.stims(b,t)~=P.clickside(b,t) && P.clickside(b,t)<99;
                P.cor(b,t) = 0;
            elseif P.clickside(b,t)==99;
                P.cor(b,t) = 0;
            end
        end
        
        
        %% update staircase
        if P.stairtrain == 1 | P.stairtrain == 3;

            P.Staircase(1) = StaircaseTrial(1, P.Staircase(1), P.cor(b,t));
            P.Staircase(1) = UpdateStaircase(1, P.Staircase(1), P.stepsize);
            P.signal(b,t) =  P.Staircase(1).Signal; % save staircase output
            
        end
        
        Screen('FillRect',win,screen.colbackground);
        vbl = Screen('Flip', win, vbl + waitframe);
        timing.off1(t) = vbl;
        
        WaitSecs(0.2);
        
        %% draw confidence scale
        if P.stairtrain == 2 | P.stairtrain == 3;

            P = vertical_vas_trigger_gtec(P, b,t, screen.rect,trigQ2,trigM2,trigR2,s,dosendtriggers,CrossWidth);
            
        end
        
        
    end
    %% end of block
    if P.stairtrain==3 && b<max(P.design.blocknumber);
        % save on each block in case something crashes
        save(['./data/data_' num2str(P.suj.number) '_' P.suj.sex '_' num2str(P.suj.age) '_' num2str(P.stairtrain) '_tmp.mat'],'P');
        Screen('FrameArc', win, frame_blue, criterionRect,  90-crit_deg-P.rdk.minCritFrame, -P.rdk.sizeCritFrame, P.rdk.frameThick, P.rdk.frameThick);
        Screen('FrameArc', win, frame_red,  criterionRect,  90-crit_deg+P.rdk.minCritFrame,  P.rdk.sizeCritFrame, P.rdk.frameThick, P.rdk.frameThick);
        Screen('DrawLines', win, criterionLines, P.rdk.fixRadPix, 0, screen.center, 0);
        DrawFormattedText(win, 'Vous pouvez faire une pause et appuyer sur une touche pour recommencer', 'center',100,255);
        DrawFormattedText(win, 'Pensez � evaluer votre confiance avec precision','center', [200],[250 200 75],255);
        DrawFormattedText(win, 'en utilisant toute la hauteur de l''echelle','center', [250],[250 200 75],255);
        
        Screen('Flip', win);
        
        [secs, keyCode]=KbStrokeWait;
    end
end

% plot staircase results
if P.stairtrain==1;
    figure
    plot(P.signal(1,:));
end

%% SAVE ALL DATA & QUIT
if P.stairtrain == 0 && mean(P.cor(b,t)) >= 0.9;
    Screen('DrawText',win, 'Cet entrainement est termine', 200, 200,255);
elseif P.stairtrain == 0 && mean(P.cor(b,t)) < 0.9;
    Screen('DrawText',win, 'Veuillez faire appel a l''experimentateur', 200, 200,255);
elseif P.stairtrain == 1;
    Screen('DrawText',win, 'Cette partie de l''experience est terminee', 200, 200,255);
elseif P.stairtrain == 2 && round(mean(P.VAS.confidence(b,(P.design.ntrials/2+1):P.design.ntrials)), 2) < 0.40 ...
        || round(mean(P.VAS.confidence(b,(P.design.ntrials/2+1):P.design.ntrials)), 2) > 0.60;
    Screen('DrawText',win, 'Veuillez faire appel a l''exprimentateur', 200, 200,255);
elseif P.stairtrain == 2 && round(mean(P.VAS.confidence(b,(P.design.ntrials/2+1):P.design.ntrials)), 2) >= 0.40...
        || round(mean(P.VAS.confidence(b,(P.design.ntrials/2+1):P.design.ntrials)), 2) <= 0.60;
    Screen('DrawText',win, 'Cet entrainement est termine', 200, 200,255);
elseif P.stairtrain == 3;
    Screen('DrawText',win, 'L''experience est terminee, merci pour votre participation', 200, 200, 255);
end

Screen('Flip', win);
WaitSecs(2);
P.suj.enddate = clock;

% save final data
save(['./data/data_' num2str(P.suj.number) '_' P.suj.sex '_' num2str(P.suj.age) '_' num2str(P.stairtrain) '_' datestr(now,1) '.mat'],'P','screen')


sca
if P.stairtrain==3;
    clear
end
ShowCursor;

