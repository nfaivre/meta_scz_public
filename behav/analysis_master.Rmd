---
title: "Confidence in perceptual decision-making is preserved in schizophrenia"
output:
  html_document:
    theme: united
    toc: yes
    toc_float: yes
  pdf_document:
    toc: yes
---

Analyze behavioral and mousetracking data from scz patients and controls  
See: https://gitlab.com/nfaivre/meta_scz_public.git

# R setup
(not shown here)
```{r setup, warning=F, message=F, include=FALSE}
rm(list = ls(all = TRUE))
library(plyr); library(here)
# clear workspace
## load libraries
source(here('functions', 'loadlibs.R'))
source(here('functions', 'Function_metad_group.R'))
source(here('functions', 'computeROC.R'))
source(here('functions', 'sdt.R'))

cluster = new_cluster(6)
cluster %>% cluster_library(c('dplyr', 'afex', 'purrr', 'metaSDT')) # add relevant libraries to different clusters 
cluster_copy(cluster, 'ci')

# chose whether to redo preproc or not
redo_preproc = 0
# plotting parameters
theme_set(
  theme_bw(base_size = 10) + theme(
    axis.line = element_line(colour = "black"),
    # panel.grid.major = element_blank(),
    panel.grid.minor = element_blank(),
    panel.border = element_blank(),
    panel.background = element_blank(),
    strip.background = element_blank()
  )
) # set ggplot theme to b&w

# change default color
scale_colour_discrete <-
  function(...) {
    scale_colour_brewer(..., palette = "Set2")
  }
scale_fill_discrete <-
  function(...) {
    scale_fill_brewer(..., palette = "Set2")
  }

```

`r opts_chunk$set(cache=T) # use cache`

# Prepare and load data 
(not shown here)
```{r, load & preproc data, , echo=F,warning=F,message=F}
if (redo_preproc==1) {
  ## LOAD DATA
  # source(here('subscripts', 'loaddata.R')) # raw data not shared
  download.file('https://mycore.core-cloud.net/index.php/s/YcgRnhmR84tzkzx/download',
                here('..','data','open_data.rds'))
  
  a = readRDS(here('..','data','open_data.rds'))
  
  # exclude RTs hard coded (cutoffs, as per experimental design)
  liminf = 0.2
  limsup = 6
  liminf2 = 0.2
  limsup2 = 6
  a$rm = ifelse(a$rt < liminf | a$rt > limsup, 1, 0)
  a$rm2 = ifelse(a$rt2 < liminf2 | a$rt2 > limsup2, 1, 0)
  a %>% group_by(suj) %>% summarise_at(vars(rm, rm2), lst(mean)) %>% 
    summarise_at(vars(rm_mean, rm2_mean), lst(mean, ci)) # trial exclusion
  a = a %>% filter(rm == 0, rm2 == 0) %>% select(-rm, -rm2)
  
  # scale confidence rt & signal for mixed modeling
  a = a %>% group_by(suj) %>% mutate(
    zconf = scale(conf),
    zrt = scale(rt),
    zrt2 = scale(rt2),
    zsignal = scale(signal)
  )
  
  ## perform averaging on variables of interest
  indivdata = aggregate(cbind(signal, cor, rt, conf, rt2) ~ suj + group,a,mean)
  indivhits = aggregate(cor ~ suj, a[a$stim == 'left', ], mean)
  indivhits$cor = indivhits$cor - 0.01
  indivfas = aggregate(cor ~ suj, a[a$stim == 'right', ], mean)
  indivfas$cor = 1 - indivfas$cor
  indivfas$cor = indivfas$cor + 0.01
  indivdata$crit = criterion(indivhits$cor, indivfas$cor)
  indivdata$dp = dprime(indivhits$cor, indivfas$cor)
  indivdata$beta = beta(indivhits$cor, indivfas$cor)
  
  # create quartiles of confidence for sdt analysis
  bins = 4
  a = a %>% group_by(suj) %>% mutate(quantconf = ntile(conf, bins))
  
  ### COMPUTE METAPERFORMANCE METRICS
  source(here('subscripts', 'metaperf.R'))
  
  ## MOUSE PROCESSING
  source(here('subscripts', 'mouselock.R'))
  
  ## SAVE PREPROCESSED DATA
  save(
    a,
    indivdata,
    mcmc,
    Hmeta_c,
    Hmeta_p,
    goodconf,
    m,
    mlock,
    file = here('..','data','preprocessed_data.RData')
  )
  
}
if (redo_preproc == 0) {
  load(here('..', 'data', 'preprocessed_data.RData'))
}  

```
## Demographics (commented as age & gender are not shared)
```{r, demographics, echo=F}
## check number of subjects per group
# demo = indivdata %>% mutate(sex = substr(suj, nchar(suj), nchar(suj))) %>%
#   group_by(group) %>% summarise(
#     n = n(),
#     n_male = length(sex[sex == 'h']),
#     age_mean = mean(age),
#     age_ci = sd(age)
#   ) %>% mutate_if(is.numeric, round, 2) %>% print()
# 
# # check gender between groups
# a %>% mutate(sex = tolower(sex)) %>% group_by(group, suj, sex) %>% 
#   summarise() %>% group_by(group, sex) %>% summarise(count = n()) %>%
#   pull(count) %>% matrix(ncol = 2) %>% chisq.test()
```

## Adaptive staircases
```{r, demo, echo=F,warning=F,message=F}
## check staircases 
ggplot(a,aes(x=trial,y=signal)) + geom_point(aes(color=cor),size=.8)  + facet_wrap(~suj)
ggplot(a,aes(x=trial,y=signal,group=suj)) + geom_path(size=.6,alpha=.4) + facet_wrap(~group) + theme(legend.position='none') +  labs(title='Adaptive Staircase')
```

## Descriptive plots
A few variables of interest: red means evidence for absence of a difference, green means evidence for a difference, grey means the data is inconclusive (based on Bayes factors)
```{r descriptive, echo=F,warning=F,message=F}
voi = c('broc','cor','crit','da','conf','rt','signal','rt2','M_ratio_H') # variables of interest
labeller=c(`broc` = 'Confidence bias',`cor` = 'Accuracy',`crit` = 'Criterion',`da` = 'D-prime',`conf` = 'Confidence', `rt` = 'Reaction time',`signal` = 'Perceptual difficulty',`rt2` = '2nd order reaction time',`M_ratio_H` = 'M-Ratio')

avgdata = indivdata %>%  group_by(group) %>% summarise_at(voi,lst(mean,ci)) %>% select(c(sort(current_vars()))) %>%  mutate_if(is.numeric, round, 2)

tmp =indivdata %>% select(suj,group,voi) %>% gather(var,val,voi) %>% na.omit()
stats = tmp %>% group_by(var) %>% do(tidy(t.test(val~group,paired=F,data=.))) %>% select(var,p.value,statistic,parameter) %>% na.omit()
statsBF = tmp %>% group_by(var) %>% do(as.data.frame(ttestBF(formula=val~group,paired=F,rscale=0.7,data=.))) %>% select(var,bf)
stats$BF = statsBF$bf

tmp = full_join(tmp,stats)
tmp$sig = ifelse(tmp$BF<0.34,'H0',ifelse(tmp$BF>3,'H1','inconclusive'))

tmp %>% filter(p.value<1) %>%   ggplot(aes(x=group,y=val,color=sig)) +
  geom_boxplot(outlier.shape = NA)+
  geom_dotplot(position=position_dodge(width=.8),dotsize=1,alpha=.8,binaxis="y", stackdir="center") +# theme(legend.position='none')+
  scale_color_manual(values=c('red','dark green','gray' ))+
  facet_wrap(~var,scales='free',labeller = as_labeller(labeller))

```

## MCMC Hierarchical M_ratio
(see https://github.com/metacoglab/HMeta-d)  

Confirms absence of difference in meta-performance between patients and controls
```{r mcmc, echo=F,warning=F,message=F}
## plot MCMC for M_ratio_H
mean_c = exp(summary(Hmeta_c)$statistics[,'Mean'] %>%as.vector() %>% nth(.,-2))
mean_p = exp(summary(Hmeta_p)$statistics[,'Mean'] %>%as.vector() %>% nth(.,-2))

sd_c = exp(summary(Hmeta_c)$statistics[,'SD'] %>%as.vector() %>% nth(.,-2))
sd_p = exp(summary(Hmeta_p)$statistics[,'SD'] %>%as.vector() %>% nth(.,-2))


HDP_c = HPDinterval(Hmeta_c, prob = 0.95) %>% as.data.frame() %>%   rownames_to_column(var = "name") %>% filter(name=='mu_logMratio')
HDP_c = round(c(exp(mean(as.numeric(HDP_c[grep('upper',names(HDP_c))]))), exp(mean(as.numeric(HDP_c[grep('lower',names(HDP_c))])))),2)

HDP_p = HPDinterval(Hmeta_p, prob = 0.95) %>% as.data.frame() %>%   rownames_to_column(var = "name") %>% filter(name=='mu_logMratio')
HDP_p = round(c(exp(mean(as.numeric(HDP_p[grep('upper',names(HDP_p))]))), exp(mean(as.numeric(HDP_p[grep('lower',names(HDP_p))])))),2)

## HDP difference
diff = mcmc %>% select(-Parameter) %>% pivot_wider(names_from=group,values_from=value) %>% mutate(diff=Control-Schizophrenia)
# bayestestR::hdi(diff$diff,ci=0.95)
# bayestestR::equivalence_test(diff$diff, range = c(-0.5, 0.5),ci=0.89)
# bayestestR::p_direction(diff$diff)
prior <- distribution_normal(1000, mean = 0.2, sd = 1)
BF_Mratio=bayestestR::bayesfactor_parameters(posterior=diff$diff,prior=prior)

plot_mratio <-
  mcmc %>%
  ggplot(aes(exp(value),fill=group)) +
  geom_density(alpha = 0.4,position='identity',color='black')+
  # geom_vline(xintercept=c(mean_c,mean_p),linetype='dashed',size=1.5) +
  geom_segment(aes(x = HDP_p[1], y = .4, xend = HDP_p[2], yend = .4), size = 2,color=brewer.pal(n=3,name="Set2")[2])+
  geom_segment(aes(x = HDP_c[1], y = .2, xend = HDP_c[2], yend = .2), size = 2,color=brewer.pal(n=3,name="Set2")[1])+
  labs(title='B.',x="M-ratio posterior estimate",y='Density')+
  theme(legend.position=c(0.2,0.9),legend.title = element_blank()) 

```


## GLM analysis
Another strategy to assess metaperformance, with mixed logistic regression. Still shows no difference between groups 
```{r glmer, echo=F,warning=F,message=F}

# bayesian version
chains=4
iter=10000
warmup = 2000

priors <- c(set_prior("normal(2,5)", class = "b", coef= "zconf"),
            set_prior("normal(0,5)", class = "b", coef= "groupSchizophrenia"),
            set_prior("normal(-1,5)", class = "b", coef= "zconf:groupSchizophrenia"))

priors_null <- c(set_prior("normal(2,5)", class = "b", coef= "zconf"),
                 set_prior("normal(0,5)", class = "b", coef= "groupSchizophrenia"))

m2 = brm(cor ~ zconf * group + (zconf | suj), data = a, prior=priors,family = brms::bernoulli(), sample_prior=T,
         chains=chains,iter=iter,warmup = warmup, cores=chains,save_all_pars = TRUE,
         file=here('..','data','models','glm_cor-conf-group')) 

h <- c("zconf*group" = "zconf:groupSchizophrenia  = 0")
(hyp=hypothesis(m2, h))

# average fit
tmp=plot_model(m2,type = "pred",terms=c('zconf [all]','group'))

a$conf_round=round(a$zconf)
roundcor = a %>%  group_by(suj,group,conf_round) %>% summarise(cor=mean(as.numeric(cor)-1),length=n()) %>% 
  na.omit() %>% group_by(group,conf_round) %>% summarise_at(vars(cor,length),lst(mean,ci)) 

glmplot = tmp +  geom_hline(yintercept=0.5,linetype='dashed',alpha=.5)+ geom_vline(xintercept=0,linetype='dashed',alpha=.5)+
  geom_pointrange(data=roundcor,inherit.aes = F, aes(x=conf_round, y=cor_mean, ymin=cor_mean-cor_ci, ymax=cor_mean+cor_ci, size=length_mean, color=group,fill=group),position=position_dodge(width=.5),shape=19,alpha=.3) + 
  scale_size_continuous(range=c(.2,1)) + 
  labs(title='C.',x='Standardized confidence',y='First-order accuracy')+
  scale_y_continuous(breaks=seq(0.4,1,0.1))  +   scale_x_continuous(breaks=c(-2,0,2))  + 
  coord_cartesian(ylim=c(0.3,1),xlim=c(-2.5,2.5))+
  theme(legend.position='none')+
  scale_color_manual(values=brewer.pal(n=3,name="Set2")[1:2])+
  scale_fill_manual(values=brewer.pal(n=3,name="Set2")[1:2])+
  guides(size = FALSE)


```


## CONF~RT
Assess the influence of reaction times on confidence. Main effect of conf, interaction with group & cor
```{r lmer RT, echo=F,warning=F,message=F}
chains=4
iter=10000
warmup = 2000

m5 = brm(conf ~ zrt * cor * group * zsignal + (zrt + cor + zsignal | suj), data =a, 
sample_prior=T,save_all_pars = T, chains=chains, warmup = warmup,
iter=iter, cores=chains, file=here('..','data','models','lm_conf-zrt-cor-group-zsignal')) # 
summary(m5)

h <- c("zrt" = "zrt  < 0",
       "zrt:cor1" = "zrt:cor1 < 0",
       "group"= "groupSchizophrenia > 0",
       "zrt*group" = "zrt:groupSchizophrenia > 0", 
       "zsignal" = "zsignal<0")

(hyp=hypothesis(m5, h))

a$rt_round=round(a$zrt)
roundconf = a %>%  group_by(suj,group,rt_round,cor) %>% summarise(conf=mean(conf),length=n()) %>% 
  na.omit() %>% group_by(group,rt_round,cor) %>% summarise_at(vars(conf,length),lst(mean,ci)) %>% 
  select(group,x=rt_round,predicted=conf_mean,facet=cor,conf_ci,length_mean,length_ci)

plot_rt = plot_model(m5,type='pred',terms=c('zrt','group','cor'))
tmp = as_tibble(plot_rt$data)
plot_rt = ggplot(tmp,aes(x,predicted,ymin=conf.low,ymax=conf.high,fill=group,color=group)) + 
  geom_ribbon(color=NA,alpha=.2) +  geom_line() +
  geom_pointrange(data=roundconf,inherit.aes = F, aes(x=x, y=predicted, ymin=predicted-conf_ci, ymax=predicted+conf_ci, size=length_mean, color=group,fill=group),position=position_dodge(width=.5),shape=19,alpha=.3) + 
  scale_size_continuous(range=c(.1,1),name='# trials')+
  coord_cartesian(ylim=c(0.5,1),xlim=c(-2.5,2.5))+ 
  facet_wrap(~facet,labeller=as_labeller(c(`0` = 'Incorrect responses',`1` = 'Correct responses'))) + 
  theme(legend.position ='bottom') +
  labs(x='Standardized reaction times', y = 'Confidence',title='D.',color='',fill='') + scale_x_continuous(breaks=c(-2,0,2))
```


## CONF~RT2
Assess the influence of 2nd order reaction times on confidence. Main effect of conf, interaction with group & cor
```{r lmer RT2, echo=F,warning=F,message=F}
m5_2= brm(conf ~ zrt2 * cor * group * zsignal + (zrt2 + cor + zsignal | suj), data =a, 
sample_prior=T,save_all_pars = T, chains=chains, iter=iter,warmup = warmup,
 cores=chains, file=here('..','data','models','lm_conf-zrt2-cor-group-zsignal')) # 
summary(m5_2)

h <- c("zrt2" = "zrt2  < 0",
       "zrt2:cor1" = "zrt2:cor1 < 0",
       "group"= "groupSchizophrenia > 0",
       "zrt2*group" = "zrt2:groupSchizophrenia > 0")

(hyp=hypothesis(m5_2, h))

a$rt2_round=round(a$zrt2)
round2conf = a %>%  group_by(suj,group,rt2_round) %>% summarise(conf=mean(conf),length=n()) %>% 
  na.omit() %>% group_by(group,rt2_round) %>% summarise_at(vars(conf,length),lst(mean,ci)) %>% 
  select(group,x=rt2_round,predicted=conf_mean,conf_ci,length_mean,length_ci)

plot_rt2 = plot_model(m5_2,type='pred',terms=c('zrt2','group'))
tmp = as_tibble(plot_rt2$data)
plot_rt2 = ggplot(tmp,aes(x,predicted,ymin=conf.low,ymax=conf.high,fill=group,color=group)) + 
  geom_ribbon(color=NA,alpha=.2) +  geom_line() +
  geom_pointrange(data=round2conf,inherit.aes = F, aes(x=x, y=predicted, ymin=predicted-conf_ci, ymax=predicted+conf_ci, size=length_mean, color=group,fill=group),position=position_dodge(width=.5),shape=19,alpha=.3) + 
  scale_size_continuous(range=c(.1,1),name='# trials')+
  coord_cartesian(ylim=c(0.5,0.8),xlim=c(-1,5))+  theme(legend.position ='bottom') +
  labs(x='Standardized second-order reaction times', y = 'Confidence',title='',color='',fill='')
ggsave(filename=here('..','..','figures','behav','FigureS2.pdf'),plot=plot_rt2)

```


## Mouse trajectories  

Example of single traces and averages across confidence levels

```{r traj, echo=F,warning=F,message=F, include=F}
# group trajectories
praw_group = mlock %>% group_by(suj,trial,sample,group) %>% filter(sample>0,Y>0,Y<250,Xa<250,mvonset>0.2) %>% 
  ggplot(aes(X,Y,group=interaction(trial,suj),alpha=factor(chmind),color=factor(chmind))) + 
  geom_vline(xintercept = 0,alpha=.5,linetype='dashed') +
  geom_path() +  labs(x='Horizontal position',y='Vertical position',title='A.') + 
  scale_color_manual(values=c('black','dark red')) + scale_alpha_manual(values=c(0.01,.05)) +
  theme(legend.position = 'none',plot.title = element_text(vjust=-2)) +
  facet_wrap(~group) 



```

## check if the slope X~Y of the trajectory is a predictor of confidence  
```{r slope, echo=F,warning=F,message=F}
lmod = mlock %>% filter(sample>0,Xa<200,Y<200) %>% group_by(suj,trial) %>%  do(model=lmodel2(X ~ Y ,data = .))

tmp=list()
for (l in 1:dim(lmod)[1]) {
  tmp[[l]]=data.frame(slope=abs(lmod$model[[l]]$regression.results$Slope[2]),r.squared=lmod$model[[l]]$rsquare)
}
tmp=bind_rows(tmp)
tmp$suj=lmod$suj;tmp$trial=lmod$trial
s = full_join(a,tmp) %>%  filter(slope>0.2,slope<2)
slock = full_join(mlock,tmp) %>%  filter(slope>0.2,slope<2)

s = s %>% group_by(suj) %>% mutate(slope_z=scale(slope))
# slope
ms = lmer(slope_z~zconf*group*cor+(zconf+cor|suj),data=s)
anova(ms)

# squared
mr = lmer(r.squared~zconf*group*cor+(zconf+cor|suj),data=s)
anova(mr)

pslope_conf_group = slock %>%  gather(var,val,c('slope','r.squared')) %>% 
  group_by(triconf,group,suj,var) %>% summarise(val=mean(val,na.rm=T)) %>% 
  ggplot(aes(factor(triconf),val,color=factor(triconf))) + 
  geom_point(position=position_jitter(width = .15,height = 0),alpha=.15,size=1)+
  stat_summary(fun.data=mean_cl_normal,geom='pointrange') + 
  scale_color_manual(values=c('Red','Orange','Green4')) + 
  scale_x_discrete(labels=c('Low','Medium','High'))+
  theme(legend.position='none',plot.title = element_text(vjust=-2)) + 
  labs(y='Parameter estimate',x='Confidence quantile',title='C.')  +
  facet_wrap(group~var,nrow=1,ncol=4,
             labeller= as_labeller(c(`Schizophrenia` = '',`Control` = '',`r.squared` = 'R²',`slope` = 'ß'))) +
  coord_cartesian(ylim = c(0.75,1))

```

## Assess the link between kinematics and confidence over time for each group
```{r kintime, echo=F,warning=F,message=F}
## baseline correction
bl = mlock %>% filter(time < 0,mvonset>0.2) %>% group_by(suj,triconf) %>% summarise_at(vars(accXY_z,speedXY_z),lst(mean))
mlock = full_join(mlock,bl,by=c('suj','triconf'))
mlock$speedXY_z=mlock$speedXY_z-mlock$speedXY_z_mean
mlock$accXY_z=mlock$accXY_z-mlock$accXY_z_mean

# Run mixed model
allres <- mlock %>% filter(time>0,time<0.5,mvonset>0.2) %>% 
  select(trial,zconf,accXY_z,speedXY_z,suj,time,group, rt, signal, triconf) %>%
  gather(kin,val,c("accXY_z",'speedXY_z')) %>% mutate(kin=factor(kin, levels=c('speedXY_z','accXY_z')))%>% # 'accXa','accY'
  nest(-time, -kin, -group) %>% 
  mutate(lme_out = map(data, ~mixed(val~zconf + (zconf|suj),data = as.data.frame(.x),method='S',progress=F))) %>%
  mutate(perf = map(lme_out,"anova_table")) %>%
  mutate(F = map(perf,'F'), p = map(perf,'Pr(>F)')) %>% 
  mutate(F = map(F, ~.x[1]),p = map(p, ~.x[1])) %>% #just the first value corresponding to conf
  mutate(F = unlist(F),p=unlist(p)) %>% 
  select(-perf, -lme_out,-data) %>% 
  group_by(time,group, kin) %>%
  mutate(pCrit = 0 + (p <= 0.05)) %>% mutate(pCrit = na_if(pCrit, 0)) %>% 
  mutate(critFDR = 0+(p.adjust(p, "fdr") <= 0.05)) %>% mutate(critFDR = na_if(critFDR, 0))

# create time series of kinematics for each subject 
dat2plot = mlock %>% filter(time> -0.25,time<0.5) %>%  group_by(time,suj,group,triconf) %>% partition(cluster) %>% 
  summarize_at(vars(accXY_z,speedXY_z),funs(mean)) %>% collect() %>% 
  gather(kin,val,c("accXY_z",'speedXY_z')) #,'accXa','accY'
# dat2plot$kin=ifelse(dat2plot$kin=='speedXY','speedXY_z','accXY_z') # rename as _z to facilitate plotting
dat2plot = dat2plot %>% mutate(kin=factor(kin, levels=c('speedXY_z','accXY_z')))

dat2plot=full_join(dat2plot,allres,by=c('time','group','kin'))

critplot <- dat2plot %>% 
  group_by(time,suj,group,triconf, kin, pCrit, critFDR) %>% summarise(val = mean(val)) %>%  na.omit(kin) 
critplot$critFDR = ifelse(critplot$kin=='speedXY_z',0,-10)
p_mouse2b = dat2plot  %>% 
  ggplot(aes(x=time,y=val,fill=triconf,color=triconf,group=triconf)) + 
  stat_summary(fun.data = mean_cl_normal, geom = "ribbon",color=NA,alpha=.1)+ 
  stat_summary(fun.y = mean, geom = "line") + 
  labs(x='Time from movement onset (s)',y='',title='D.',fill='Conf quantile',color='Conf quantile') + 
  geom_vline(xintercept = 0,alpha=.5,linetype='dashed') +
  theme(legend.position = c(.9,.9),plot.title = element_text(vjust=-2),strip.placement = 'outside') +
  geom_point(data=critplot, aes(x=time, y=critFDR), size=2,pch=15, color='gray') +
  scale_fill_manual(values=c('Red','Orange','Green4'),labels=c('Low','Medium','High')) +
  scale_color_manual(values=c('Red','Orange','Green4'),labels=c('Low','Medium','High'))+
  facet_grid(kin~group,scales='free', switch="y",
             labeller=as_labeller(c(`speedXY_z` = 'Velocity (°/s)',`accXY_z` = 'Acceleration (°/s²)',
                                    `Schizophrenia` = "Schizophrenia", `Control`='Control'))) 
p_mouse2b


```


## Assess the link between kinematics and confidence over time (interaction with group)
```{r kintimegroup, echo=F,warning=F,message=F}
# # Run mixed model with interaction by group
allres <- mlock %>% filter(time>0,time<0.5) %>% 
  select(trial,zconf,accXY_z, speedXY_z,suj,time,group) %>%
  gather(kin,val,c('accXY_z', 'speedXY_z')) %>% # 
  nest(-time, -kin) %>%
  mutate(lme_out = map(data, ~mixed(val~ zconf * group + (1|suj),data = as.data.frame(.x),method='S',progress=F))) %>%
  mutate(perf = map(lme_out,"anova_table")) %>%
  mutate(F = map(perf,'F'), p = map(perf,'Pr(>F)')) %>% select(-lme_out,-perf,-data)

Fs=as_tibble(matrix(unlist(allres$F),ncol=3,byrow = T),.name_repair="minimal");
names(Fs) = c('zconf','group','inter')
ps=as_tibble(matrix(unlist(allres$p),ncol=3,byrow = T),.name_repair="minimal");
names(ps) = c('pzconf','pgroup','pinter')
widestats = allres %>% select(-F,-p) %>% as_tibble()
widestats = cbind(widestats,Fs,ps) 
longstats = widestats  %>%   pivot_longer(names_to = 'effect', cols =  c('zconf','group','inter')) %>% select(time,effect,F=value,kin)
tmp = widestats  %>%   pivot_longer(names_to = 'effect', cols =  c('pzconf','pgroup','pinter')) %>% select(time,effect,p=value,kin)
longstats$p=tmp$p
longstats = longstats %>% group_by(effect,kin) %>% mutate(pfdr = 0+p.adjust(p,method='fdr')<=0.05) %>% mutate(pfdr = na_if(pfdr, 0))

widestats = longstats %>% pivot_wider(names_from=effect,values_from =c(F,p,pfdr))

# create time series of kinematics for each subject 
dat2plot = mlock %>% filter(time> -0.25,time<0.5) %>%  group_by(time,suj,group,triconf) %>% partition(cluster) %>% 
  summarize_at(vars(accXY_z,speedXY_z),funs(mean)) %>% collect() %>% 
  gather(kin,val,c("accXY_z",'speedXY_z')) #,'accXa','accY'
# dat2plot$kin=ifelse(dat2plot$kin=='speedXY','speedXY_z','accXY_z') # rename as _z to facilitate plotting

dat2plot=full_join(dat2plot,widestats,by=c('time','kin'))

dat2plot = dat2plot %>% mutate(kin=factor(kin, levels=c('speedXY_z','accXY_z')))

# plot data
p_mouseS1 = dat2plot  %>% filter(time> -0.25,time<0.5) %>%
  ggplot(aes(x=time,y=val,fill=triconf,color=triconf,group=triconf)) +
  stat_summary(fun.data = mean_cl_normal, geom = "ribbon",color=NA,alpha=.2)+
  stat_summary(fun.y = mean, geom = "line") +
  facet_wrap(~kin, labeller=as_labeller(c(`speedXY_z` = 'Velocity (°/s)',`accXY_z` = 'Acceleration (°/s²)')))  +
  scale_fill_manual(values=c('Red','orange','Green4'),labels=c('Low','Medium','High')) +
  scale_color_manual(values=c('Red','orange','Green4'),labels=c('Low','Medium','High'))+
  labs(x='Time from movement onset (s)',y='Kinematics',fill='Conf quantile',color='Conf quantile') +
  geom_vline(xintercept = 0,alpha=.5,linetype='dashed') +
  theme(legend.position =c(.95,.9))+guides(color=FALSE)+
  geom_point(aes(x=time, y=pfdr_zconf-8), size=3,pch=15, color='gray') + 
  annotate(geom='text',label='Confidence',y=-7,x=-.15)+
  geom_point(aes(x=time, y=pfdr_inter-10), size=3,pch=15, color='gray') + 
  annotate(geom='text',label='Confidence*Group',y=-9,x=-.15)
ggsave(filename=here::here('..','..','figures','behav','FigureS1.pdf'),plot=p_mouseS1)

```
## Changes of mind
```{r chmind, message=FALSE, warning=FALSE}
## Test differences in CoM
nchmind = mlock %>% filter(sample==0,chmind==T) %>% group_by(suj,group,.drop=F) %>% summarise(chmind=n())
nall = mlock %>% filter(sample==0) %>% group_by(suj,group,.drop=F) %>% summarise(n=n())
nchmind = full_join(nchmind,nall,by=c('suj','group')) %>% mutate(chmind = replace_na(chmind, 0),chprop = 100*chmind/n) 

nchmind %>% group_by(group) %>% summarise_at(vars(chprop),lst(mean,ci))
t.test(nchmind$chprop~nchmind$group,paired=F)
ttestBF(formula=chprop~group,data=nchmind,paired=F)

# mlock = mlock %>% group_by(suj) %>% mutate(zsignal=scale(signal))
msig = mlock %>% filter(sample==0) %>% lmer(zsignal~group*chmind + (chmind|suj),data=.)
anova(msig)

mons = mlock %>% filter(sample==0) %>% lmer(mvonset~group*chmind + (chmind|suj),data=.)
anova(mons)

mcor = mlock %>% filter(sample==0) %>% glmer(cor~group*chmind + (chmind|suj),family = 'binomial',data=.)
summary(mcor)

mconf = mlock %>% filter(sample==0) %>% lmer(zconf~group*chmind + (chmind|suj),data=.)
anova(mconf)


chmind_avg = mlock %>% filter(sample==0) %>% mutate(cor=as.numeric(cor)-1) %>% group_by(suj,group,chmind,.drop=F) %>%
  summarise_at(vars(cor,conf),lst(mean)) %>% 
  pivot_longer(cols=c('cor_mean','conf_mean'))  %>%
  mutate(name=factor(name, levels=c('cor_mean','conf_mean')))

pchmind = chmind_avg %>% ggplot(aes(chmind,value,color=chmind)) + stat_summary(fun.data=mean_cl_normal,geom='pointrange',size=.6) +
  geom_point(position=position_jitter(height=0,width=.15),alpha=.15,size=.8) +
  labs(x='Change of mind',y='Mean value',title='B.') + scale_color_manual(values=c('black','dark red')) + 
  theme(legend.position='none',plot.title = element_text(vjust=-2)) +
   facet_wrap(group~name,ncol=4,nrow=1,
               labeller= as_labeller(c(`Schizophrenia` = '',`Control` = '',
                                       `conf_mean` = 'Confidence',`cor_mean` = 'Accuracy'))) +
  scale_x_discrete(labels=c('False','True'))


## check reaction times in trials with no CoM (same model as m5 above but with chmind==0)
chains=4
iter=10000
warmup = 2000

m5 = brm(conf ~ zrt * cor * group * zsignal + (zrt + cor + zsignal | suj), data =mlock[mlock$sample==0 & mlock$chmind==0,],
         sample_prior=T,save_all_pars = T, chains=chains, iter=iter, cores=chains,warmup = warmup,
         file=here::here('..','data','models','lm_conf-zrt-cor-group-zsignal-noCoM')) # 
summary(m5)

h <- c("zrt" = "zrt  < 0",
       "zrt:cor1" = "zrt:cor1 < 0",
       "group"= "groupSchizophrenia > 0",
       "zrt*group" = "zrt:groupSchizophrenia > 0", 
       "zsignal" = "zsignal<0")

(hyp=hypothesis(m5, h))
```

```{r final figs, message=FALSE, warning=FALSE}
(fig1 = (plot_mratio + glmplot) / plot_rt)
ggsave(filename=here::here('..','figures','Figure1.pdf'),plot=fig1,width=8,height=8)
ggsave(filename=here::here('..','figures','Figure1.png'),plot=fig1,width=8,height=6)

fig2A = grid.arrange(praw_group,pchmind,pslope_conf_group,nrow=3) 
fig2B = p_mouse2b
  
fig2 = grid.arrange(fig2A, p_mouse2b, nrow = 1) #patchwork doesn't work here
ggsave(filename=here::here('..','figures','Figure2.pdf'),plot=fig2,width = 12,height=8)

```


# Psychometric data (script shared but no data)
```{r questionaire, echo=T,warning=F,message=F}
# source(here('subscripts','quest_analysis.R'))
```


# Race model
```{r race model, message=FALSE, warning=FALSE}
source(here('subscripts','plot_racemodel.R'))
```
