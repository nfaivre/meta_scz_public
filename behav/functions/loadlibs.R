libs2load = c('signal','plyr', 'dplyr', 'knitr',  'metaSDT', 'RColorBrewer', 'lmerTest', 'Hmisc', 'patchwork', 'sjPlot', 'ggcorrplot', 'ggridges','multidplyr',
              'R.matlab', 'here', 'magrittr', 'reshape2', 'rjags', 'coda', 'lattice', 'broom', 'ggmcmc', 'tidyverse', 'brms', 'BayesFactor', 'bayestestR',
              'flextable', 'officer', 'rstan', 'ggstance', 'lmodel2', 'gridExtra','readr','pracma')

lapply(libs2load, library, character.only = TRUE)


# differentiation matrix
pinv <- function (A)
{
  s <- svd(A)
  # D <- diag(s$d) Dinv <- diag(1/s$d)
  # U <- s$u V <- s$v
  # A <- U D V'
  # X <- V Dinv U'
  s$v %*% diag(1/s$d) %*% t(s$u)
}

# filter function with complex numbers
filter.complex=function(x,...){complex(real=signal::filter(Re(x),...), imag=signal::filter(Im(x),...))}
