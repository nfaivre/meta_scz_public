%========================================================
%   step2_fitconf.m
%
%   Reads behavioral data files and fits a race model to
%   response times, accuracy and confidence for high and low RT
%
%   Faivre, Roger et al., 2019,
%   Confidence in perceptual decision-making is preserved in schizophrenia
%
%   preregistration: https://osf.io/84wqp/
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.fr>
%   03/12/2019
%========================================================

clear
addpath functions
config();

% load data
alldata = readtable('mouse_summary.csv');
if isempty(opts.subjects)
   opts.subjects = unique(alldata.suj).';
end

% init
data = cell(length(opts.subjects),3);
confparams = cell(1,3);
confestimation = cell(1,3);
out = cell(1,3);
ll = [];

%% Here we set the three possible ways confidence could be computed.

options = cell(1,3);
% the first model assumes that subjects compute confidence from
% the accumulator corresponding to their choice (e.g. Zylberberg et al., 2012,
% Front. Integr. Neuro.; Peters et al., 2018, Nat. Hum. Behav.;
options{1} = opts;
options{1}.confmodel = 'win';

% this model assumes that subjects compute confidence from the loosing
% accumulator (e.g. Kiani et al., 2009, Science; Kiani et al., 2014,
% Neuron).
options{2} = opts;
options{2}.confmodel = 'lose';

% this model assumes that subjects compute confidence from the balance of
% evidence: the difference between the winning and loosing accumulator
% (e.g. Rahnev et al., 2016, PNAS; Van den Berg et al., 2016, eLife)
options{3} = opts;
options{3}.confmodel = 'balance';

% for each subject
s = 0;
for subj_=opts.subjects(1:end)
    
    subj = cell2mat(subj_);
    s = s+1;
    
    % select data for this subject
    data = alldata(strcmp(alldata.suj,subj),:);
    
    %========================================================
    %% load behav data
    %========================================================
    dat.onset = data.mvonset;
    dat.dir = data.dir;
    dat.rt = data.rt;
    dat.resp = data.cor;
    dat.conf = cellfun(@str2double,data.triconf);
    dat.chmind = strcmp(data.chmind,'TRUE');
    
    % we convert from stimulus-coding to accuracy-coding
    dat.dir(data.stim == 1) = -dat.dir(data.stim == 1);
    
    %========================================================
    %% Simulate DV
    %========================================================
    
    % load parameters from the best model in terms of log-likelihood
    p = load(['models/' opts.type '/s' num2str(subj) '_race_' opts.type  '.mat'],'fitparams','opts','ll');
    
    % select best model
    [bestll,best] = max(-p.ll);
    
    % output
    fprintf('Subject: %s, ll = ',opts.subjects{s});
    for i=1:length(p.ll)
        if i==best
            fprintf('[%.3f]',-p.ll(i));
        else
            fprintf(' %.3f ',-p.ll(i));
        end
        
    end
    fprintf('\n');
    
    % get parameters from best model
    params = p.fitparams{best};
    
    % simulate dv
    sim = evacc(params,opts);
    
    %========================================================
    %% Fit to confidence
    %========================================================
    % Here, we need to transform the state of two accumulators
    
    for modeltype = 1:length(options)
%%
        rep = 0;
        for rep1 = 1:length(params.confreadout.init)
            
            rep = rep+1;
            
            params.confreadout.val =  params.confreadout.init(rep1);
            
            % for reproducibility, fix random number generator
            rng(1234);
            
            % compute confidence and readout
            [simconf,simdvint] = dv2conf( sim.dv,sign(sim.resp)>0, sim.dect, params, options{modeltype});
           
            % set initial parameters to estimate proportions of 1 and 3
            % confidence ratings
            
            if strcmp(options{modeltype}.confmodel,'lose')
                prop1 = 1-mean(dat.conf==1);
                prop2 = mean(dat.conf==3);
                q = quantile(simdvint,[prop1 prop2]);
                params.confc1.val = q(2);
                params.confc2.val = q(1);
            else
                prop1 = mean(dat.conf==1);
                prop2 = 1-mean(dat.conf==3);
                q = quantile(simdvint,[prop1 prop2]);
                params.confc1.val = q(1);
                params.confc2.val = q(2);    
            end
            
            % fit confidence
            [ confparams{rep,modeltype}, ll(rep,modeltype),out{rep,modeltype}] = ...
                fitconf( dat.onset, dat.dir>0, dat.conf, sim.resp>0, sim.dect, sim.dv,  params, options{modeltype});
            
        end
        fprintf(' * Model %s loglikelihood: %.2f (active) \n',options{modeltype}.confmodel, -min(ll(:,modeltype)));
        
    end
    
    
    save(['models/' opts.type '/s' num2str(subj) '_race_' opts.type '_confsim.mat'],...
        'confparams', 'sim','out','ll','options');
    
end