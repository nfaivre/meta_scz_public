%========================================================
%   Set parameters for the model
%
%   Faivre, Roger et al., 2019, 
%   Confidence in perceptual decision-making is preserved in schizophrenia
%
%   preregistration: https://osf.io/84wqp/
%      
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.fr>
%   03/12/2019
%========================================================

function [ param ] = paramset(  )

param = struct();

%% FREE PARAMETERS

% The decision bound (constrained to >0)
param.bound.val = 50;
param.bound.fit = 1;
param.bound.init = [30,60];
param.bound.trans = @(x) exp(x+1);
param.bound.itrans = @(x) log(x)-1;

% The drift rate (constrained to >0)
param.drift.val = 0.01;
param.drift.fit = 1;
param.drift.init = [0.001,0.01,0.1];
param.drift.trans = @(x) exp(x+1);
param.drift.itrans = @(x) log(x)-1;

% The non-decision time (constrained to >0)
param.ndtime.val = 0.2;
param.ndtime.fit = 1;
param.ndtime.init = 0.1:0.2:0.7;
param.ndtime.trans = @(x) exp(x+1);
param.ndtime.itrans = @(x) log(x)-1;

% The criteria for confidence mapping (second stage)
param.confc1.val = 0;
param.confc1.fit = 1;
param.confc1.trans = @(x) (x);
param.confc1.itrans = @(x) (x);

param.confc2.val = 10;
param.confc2.fit = 1;
param.confc2.trans = @(x) (x);
param.confc2.itrans = @(x) (x);

%% FIXED PARAMETERS
% lower bound fixed to zero
param.rbound.val = 0;
param.rbound.fit = 0;
param.rbound.trans = @(x) exp(x);
param.rbound.itrans = @(x) log(x);

% correlation between the two accumulators (as in Van den Berg et al., 2016,
% eLife)
param.rho.val = -1/sqrt(2);
param.rho.fit = 0;
param.rho.trans = @(x) (x);
param.rho.itrans = @(x) (x);

% The non-decision time variability (as in Van den Berg et al., 2016,
% eLife)
param.ndtime_std.val = 0.06;
param.ndtime_std.fit = 0;
param.ndtime_std.trans = @(x) 1./(1+exp(-x));
param.ndtime_std.itrans = @(x) log(x./(1-x));

% The time at which the post-decisional evidence is readout and mapped to confidence
param.confreadout.fit = 0;
param.confreadout.init = [0.001 0.1:0.1:1];
param.confreadout.trans = @(x) exp(1+x);
param.confreadout.itrans = @(x) log(x)-1;
end

