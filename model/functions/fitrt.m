%========================================================
%   fitrt.m fits a race model to rt and choice accuracy (rt)
%
%   Faivre, Roger et al., 2019,
%   Confidence in perceptual decision-making is preserved in schizophrenia
%
%   preregistration: https://osf.io/84wqp/
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.fr>
%   03/12/2019
%========================================================

function [ params,stat,out] = fitrt( rt,resp, params, opts)

stat = NaN;
out = [];

% censoring
bad = isnan(resp) |  rt < opts.cens(1) | rt > opts.cens(2);
resp(bad) = [];
rt(bad) = [];

% invert rt for errors, that way we can compute likelihoods separating
% correct and error choices
rt(resp<0) = -rt(resp<0);


%% Find which parameters to fit over all parameter and transform
fields = {'bound','drift','ndtime','ndtime_std','rho','rbound'};
p = 0;
param_val = [];
param_id = {};
for f=1:length(fields)
    if params.(fields{f}).fit
        p = p+1;
        % inverse transform of parameters
        param_val(p) = params.(fields{f}).itrans(params.(fields{f}).val);
        param_id{p} = fields{f};
    end
end

%% Fitting procedure
options = optimset('Display',opts.optimizationdisplay1);
[p, stat] = fminsearch(@fitfunc,param_val,options);

%% Transform parameters back
for f=1:length(param_id)
    params.(param_id{f}).val = params.(param_id{f}).trans(p(f));
end

    function neglogll = fitfunc(p)
        
        % Reset random number generator
        rng('default');
        
        % transform parameters back to "random-walk space"
        newparams = params;
        for f=1:length(param_id)
            newparams.(param_id{f}).val = newparams.(param_id{f}).trans(p(f));
        end
        
        % simulate evidence accumulation and corresponding choice accuracy, rt
        sim = evacc(newparams,opts);
        
        % censoring
        simbad = isnan(sim.resp) | sim.rt < opts.cens(1) | sim.rt > opts.cens(2);
        sim.rt(simbad) = [];
        sim.dect(simbad) = [];
        sim.resp(simbad) = [];
        sim.dv(simbad,:,:) = [];
        
        % invert RT for errors
        sim.rt(sim.resp<0) = -sim.rt(sim.resp<0);
        
        
        if sum(~simbad) < 10
            % if the accumulation process failed (e.g. too high decision
            % bound), set the P(rt) very low. Shouldn't happen too much.
            prt = eps;
        else
            % compute likelihood P(rt) with Kolmogorov-Smirnov
            [~,prt] = kstest2(sim.rt,rt.');
        end
        
        % value to minimize
        neglogll = -log(prt);
        
        % save current state
        out.ll = -neglogll;
        out.params = params;
        
        
        %% plot some stuff
        if opts.doplot
            %%
            l = 1./length(resp);
            t = linspace(0,3,opts.L/opts.dt);
            figure(100); clf;
            subplot(121); hold on;
            [k,s] = hist(-rt(resp<0),0:0.3:6);
            plot(s,l.*k,'r');
            [k,s] = hist(rt(resp>0),0:0.3:6);
            plot(s,l.*k,'g');
            lsim = 1./length(sim.resp);
            if sum(sim.resp<0) && ~all(isnan(sim.rt(sim.resp<0)))
                [k,s] = hist(-sim.rt(sim.resp<0),0:0.3:6);
                plot(s,lsim.*k,'r--');
            end
            if sum(sim.resp>0) && ~all(isnan(sim.rt(sim.resp>0)))
                [k,s] = hist(sim.rt(sim.resp>0),0:0.3:6);
                plot(s,lsim.*k,'g--');
            end
            xlim([0,opts.L]);
            xlabel('Response time [s]');
            ylabel('Probability');
            grid on
            title(sprintf('ll: %.2f',out.ll));
            
            subplot(122); hold on;
            plot(t,mean(sim.dv(sim.resp<0,:,1),1),'r');
            plot(t,mean(sim.dv(sim.resp>0,:,2),1),'g');
            plot(t,mean(sim.dv(sim.resp<0,:,1),1),'r--');
            plot(t,mean(sim.dv(sim.resp>0,:,2),1),'g--');
            plot(xlim(),[0 0],'k','LineWidth',2);
            b = newparams.bound.val;
            fill([t(1)-0.025 t(1)+0.025 t(1)+0.025 t(1)-0.025 t(1)-0.025],[0,0,b,b,0],'k','FaceAlpha',0.2);
            bnd = newparams.bound.val;
            if length(bnd) == 1
                bnd = bnd*(t*0+1);
            end
            plot(t,bnd,'k--','LineWidth',2);
            
            xlim([0,opts.L]); ylim([-newparams.bound.val,4*newparams.bound.val])
            xlabel('Decision time [s]');
            ylabel('Mean decision variable');
            grid on
            title(sprintf('#NaNs: %d/%d',opts.N-length(sim.resp),opts.N));
            
        end
    end

end

