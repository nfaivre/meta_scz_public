%========================================================
%   evacc.m evidence accumulation
%   simulates a race accumulation
%
%   Faivre, Roger et al., 2019, 
%   Confidence in perceptual decision-making is preserved in schizophrenia
%
%   preregistration: https://osf.io/84wqp/
%      
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.fr>
%   03/12/2019
%========================================================
function [ sim ] = evacc( params, opts )

%========================================================
%% Prepare parameters
%========================================================

% number of time points to simulate
K = round(opts.L/opts.dt);

% extract parameters
v = params.drift.val; % drift rate
B = params.bound.val; % decision bound 
tnd = params.ndtime.val; % non-decision time
tnd_var = params.ndtime_std.val; % non-decision time variability
rho = params.rho.val; % correlation between accumulators
rB = params.rbound.val; % lower bound (reflection)

% simulated variables
sim.dect = nan(opts.N,1); % time of decision 
sim.resp = nan(opts.N,1); % response side (correct=1, incorrect = 0)
sim.dv = nan(opts.N,K,2); % decision variables (one per accumulator)

if opts.doprint
    fprintf('B=%.2f, rB=%.2f, rho=%.2f, v=%.3f, tnd=%.2f, tnd_std=%.3f\n',B,rB,rho,v,tnd,tnd_var);
end

% covariance matrix
sigma = [1,rho; rho, 1];

for i=1:opts.N
    
    % correlated noise
    samples = mvnrnd([-v v],sigma,K);
    
    % initial condition
    dv = zeros(K,2);
    
    % bounded accumulation
    for j=2:K
        dv(j,:) = max(dv(j-1,:) + samples(j-1,:),-rB);
    end
    
    
    % take the maximum of each accumulator
    m = max(dv,[],2);
    
    % subtract the boundary
    m = m-B;
   
    % find when the maximum of each accumulator first reaches the bound
    rt_ = find(m>0,1,'first');
    
    % if any accumulator reached the bound: 
    if ~isempty(rt_) 
        % save RT and response
        sim.dect(i) = rt_;
        % since one accumulator is close to zero (it just reached the
        % decision bount), we define the response on a continuous scale,
        % scaling it by the value of the loosing accumulator
        sim.resp(i) = dv(rt_,:)*[-1/B ; 1/B];
    end
    sim.dv(i,:,:) = dv;
end

% output in seconds
sim.dect = sim.dect*opts.dt;

% add non-decision time to decision time to create RT
sim.rt = sim.dect + tnd + tnd_var.*randn(size(sim.dect));

end

