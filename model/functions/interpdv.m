%========================================================
%   interpdv.m interpolate the decision variable in case of non-integer readout
%
%   Faivre, Roger et al., 2019, 
%   Confidence in perceptual decision-making is preserved in schizophrenia
%
%   preregistration: https://osf.io/84wqp/
%      
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.fr>
%   03/12/2019
%========================================================
function [ dvnew ] = interpdv( dv, t, opts)

    % transform from time to sample
    tconf = t/opts.dt;
    % get fraction
    tconf_w = tconf - floor(tconf);
    % floor to get integer sample
    tconf = floor(tconf);
    
    % interpolate
    dvnew = (1-tconf_w)*dv(:,tconf,:) + (tconf_w)*dv(:,tconf+1,:);

end

