%========================================================
%   dv2conf.m readout of decision process and maps to three confidence
%   values
%
%   input arguments
%       - dv, resp: the output from evacc.m
%       - rt: we readout confidence params.confreadout.val after this value
%       of rt
%       - params: the set of parameters
%       - opts: the set of config
%
%   Faivre, Roger et al., 2019,
%   Confidence in perceptual decision-making is preserved in schizophrenia
%
%   preregistration: https://osf.io/84wqp/
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.fr>
%   03/12/2019
%========================================================

function [ conf, dvconf, contconf_win,contconf_lose,contconf_bal ] = dv2conf( dv,resp, rt, params, opts)

contconf_win = nan(length(resp),2./opts.dt+1);
contconf_lose = nan(length(resp),2./opts.dt+1);
contconf_bal = nan(length(resp),2./opts.dt+1);

% linear interpolation in case of non-integer readout time
tconf_ = round(abs(rt)./opts.dt);
tconf = tconf_ + params.confreadout.val./opts.dt;
tconf_w = tconf - floor(tconf);
% floor to get integer sample
tconf = min(floor(tconf),opts.L./opts.dt-1);
dv_interp = nan(length(tconf),2);
for tr=1:length(dv_interp)
    dv_interp(tr,:) = (1-tconf_w(tr))*squeeze(dv(tr,tconf(tr),:)) + ...
        (tconf_w(tr))*squeeze(dv(tr,tconf(tr)+1,:)) ;
end

% prepare
dvconf = zeros(length(resp),1);
dvconf_win = zeros(length(resp),1);
dvconf_loose = zeros(length(resp),1);

% define wining and loosing accumulators depeding on the first-order choice

dvconf_win(resp==0,:) = dv_interp(resp==0,1);
dvconf_win(resp==1,:) = dv_interp(resp==1,2);
dvconf_loose(resp==0,:) = dv_interp(resp==0,2);
dvconf_loose(resp==1,:) = dv_interp(resp==1,1);

if strcmp(opts.confmodel,'win')
    % confidence is readout from the accumulator corresponding to the first
    % order choice
    dvconf = dvconf_win;
    
elseif strcmp(opts.confmodel,'lose')
    % confidence is readout from the loosing accumulator (we invert the sign so
    % that low evidence for the opposite choice becomes high confidence in
    % the other choice and vice-versa
    dvconf = dvconf_loose;
   
elseif strcmp(opts.confmodel,'balance')
    % confidence is readout from the "balance of evidence", i.e. the
    % difference between the winning and loosing accumulators
    dvconf = bsxfun(@times, 2*resp-1, diff(dv_interp,1,2));
%     for tr=1:length(dv_interp)
%         if tconf_(tr) > 1/opts.dt
%         else
%             x = 1./opts.dt-tconf_(tr)+1;
%         end
%     end
end
for tr=1:length(dv_interp)
    if tconf_(tr) > 1/opts.dt
        contconf_win(tr,:,:) = dv(tr,tconf_(tr)+(-1/opts.dt:1/opts.dt),resp(tr)+1);
        contconf_lose(tr,:,:) = dv(tr,tconf_(tr)+(-1/opts.dt:1/opts.dt),(resp(tr)==0)+1);
        contconf_bal(tr,:,:) = (2*resp(tr)-1)*diff(dv(tr,tconf_(tr)+(-1/opts.dt:1/opts.dt),:),1,3);    
        
    else
        x = 1./opts.dt-tconf_(tr)+1;
        contconf_win(tr,x:2/opts.dt,:) = dv(tr,1:(tconf_(tr)+1/opts.dt),resp(tr)+1);
        contconf_lose(tr,x:2/opts.dt,:) = dv(tr,1:(tconf_(tr)+1/opts.dt),(resp(tr)==0)+1);
        contconf_bal(tr,x:2/opts.dt,:) = (2*resp(tr)-1)*diff(dv(tr,1:(tconf_(tr)+1/opts.dt),:),1,3);
        
    end
end

% confidence mapping
c1 = params.confc1.val;
c2 = params.confc2.val;

conf = dvconf*0;
if strcmp(opts.confmodel,'loose')
    % in this case the mapping is reversed
    conf(dvconf <= c1) = 3;
    conf(dvconf > c1 & dvconf < c2) = 2;
    conf(dvconf > c2) = 1;
else
    conf(dvconf <= c1) = 1;
    conf(dvconf > c1 & dvconf < c2) = 2;
    conf(dvconf > c2) = 3;
end

end

