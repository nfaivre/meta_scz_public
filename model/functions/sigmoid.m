%========================================================
%   sigmoid.m passes a value through a sigmoid function, adjusting bias (b) and
%   sensitivity (a) and optionally inverts confidence for all w==-1
%
%   Faivre, Roger et al., 2019, 
%   Confidence in perceptual decision-making is preserved in schizophrenia
%
%   preregistration: https://osf.io/84wqp/
%      
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.fr>
%   03/12/2019
%========================================================

function [ simconf ] = sigmoid( dv, a,b, w)
if nargin < 2
    a = 1;
end
if nargin < 3
    b = 0;
end
if nargin < 4 || isempty(w)
    w = ones(size(dv));
end
reg = dv*a - b;

mult = bsxfun(@times,w,reg);
simconf = exp(mult)./(1+exp(mult));

end

