%========================================================
%   fitconf.m fits mapping from accumulated evidence to observed confidence
%   ratings
%
%
%   input arguments
%       - dv, resp: the output from evacc.m
%       - params: the set of parameters
%       - opts: the set of config
%       - resp_covert: (optional)
%
%   Faivre, Roger et al., 2019,
%   Confidence in perceptual decision-making is preserved in schizophrenia
%
%   preregistration: https://osf.io/84wqp/
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.fr>
%   03/12/2019
%========================================================

function [ params,stat,out] = fitconf( rt,resp,conf, simresp, simrt, simdv,  params, opts)

out = [];
% remove bad trials and censor
% (rt is only used to censor conf)

bad = isnan(resp) | rt < opts.cens(1) | rt > opts.cens(2);%| simrt < opts.cens2(1) | simrt > opts.cens2(2) ;%| conf == -1;
resp(bad) = [];
rt(bad) = [];
conf(bad) = [];

% remove bad trials and censor
simbad = isnan(simresp) | simrt < opts.cens2(1) | simrt > opts.cens2(2);%| simrt < opts.cens2(1) | simrt > opts.cens2(2) ;%| conf == -1;
simresp(simbad) = [];
simrt(simbad) = [];
simdv(simbad,:,:) = [];

% invert conf for error
conf(resp==0) = -conf(resp==0);


%% Find which parameters to fit
fields = {'confc1','confc2','confreadout'};
p = 0;
param_val = [];
param_id = {};
for f=1:length(fields)
    if params.(fields{f}).fit
        p = p+1;
        % inverse transform of parameters
        param_val(p) = params.(fields{f}).itrans(params.(fields{f}).val);
        param_id{p} = fields{f};
    end
end

%% Fitting procedure
options = optimset('Display',opts.optimizationdisplay2);
[p, stat] = fminsearch(@fitfunc,param_val,options);

%% Transform parameters back
for f=1:length(param_id)
    params.(param_id{f}).val = params.(param_id{f}).trans(p(f));
end

    function neglogll = fitfunc(p)
        
        % Reset random number generator
        rng('default');
        newparams = params;
        
        % transform parameters back to usuable values
        for f=1:length(param_id)
            newparams.(param_id{f}).val = newparams.(param_id{f}).trans(p(f));
        end
        
        %%
        % get transform d.v. to confience
        [simconf,simdvint] = dv2conf( simdv,sign(simresp)>0, simrt, newparams, opts);
        
        % invert conf for errors
        simconf(simresp==0) = -simconf(simresp==0);
        
        % compute log-likelihood
        ll(1) = log(binopdf(sum(simconf==-1),length(simresp),mean(conf==-1))+eps);
        ll(2) = log(binopdf(sum(simconf==-2),length(simresp),mean(conf==-2))+eps);
        ll(3) = log(binopdf(sum(simconf==-3),length(simresp),mean(conf==-3))+eps);
        ll(4) = log(binopdf(sum(simconf==1),length(simresp),mean(conf==1))+eps);
        ll(5) = log(binopdf(sum(simconf==2),length(simresp),mean(conf==2))+eps);
        ll(6) = log(binopdf(sum(simconf==3),length(simresp),mean(conf==3))+eps);
        
        neglogll = -sum(ll);
        
        % value to minimize
        out.ll = -neglogll;
        
        % save current state
        out.alll = ll;
        out.params = params;
        
        
        % plot some stuff
        if opts.doplot
            
            figure(1); clf;
            subplot(121); hold on;
            
            bar(-3,mean(conf==-3),'FaceColor','g');
            bar(-2,mean(conf==-2),'FaceColor',[1 0.5 0]);
            bar(-1,mean(conf==-1),'FaceColor','r');
            bar(1,mean(conf==1),'FaceColor','r');
            bar(2,mean(conf==2),'FaceColor',[1 0.5 0]);
            bar(3,mean(conf==3),'FaceColor','g');
            
            plot([-3,-2,-1,1,2,3],[mean(simconf==-3) mean(simconf==-2) mean(simconf==-1) mean(simconf==1) mean(simconf==2) mean(simconf==3)],...
                'ko','MarkerSize',10,'MarkerFaceColor','k');
            title(sprintf('logll: %.2f, t:%.3f, c1: %.2f, c2: %.2f',neglogll,...
                newparams.confreadout.val, newparams.confc1.val, newparams.confc2.val));
            
            subplot(122); hold on
            mx = max(simdvint)*1.2;
            [kcor,x] = hist(simdvint(simresp==1),-mx:2:mx);
            kerr = hist(simdvint(simresp==0),-mx:2:mx);
            plot(x,kcor,'g','LineWidth',2);
            plot(x,kerr,'r','LineWidth',2);
            c1 = newparams.confc1.val;
            c2 = newparams.confc2.val;
            plot(c1*[1 1],ylim(),'k');
            plot(c2*[1 1],ylim(),'k--');
            pause(0.01);
        end
    end

end

