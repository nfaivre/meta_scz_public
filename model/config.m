%========================================================
%   Configuration
%   
%   Faivre, Roger et al., 2019, 
%   Confidence in perceptual decision-making is preserved in schizophrenia
%
%   preregistration: https://osf.io/84wqp/
%      
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.fr>
%   03/12/2019
%========================================================

opts = struct();

% The directory containing the behavioral files 
opts.data_path = '/Users/meaperei/Dropbox/Work/Code/meta_scz/scripts/model/';

% The list of subjects to fit (TO BE DEFINED)
opts.subjects =   {};

% Give a name to your fit
opts.type = 'paper'; 

% The number of seconds to simulate after the stimulus (doesn't take
% non-decision time into account so if opts.L=6 and ndtime is fitted to 0.3 will
% simulate RTs up to 6.3 seconds after stimulus.
opts.L = 6;

% Censoring of RTs for the first-order fit (movement onset + choice)
opts.cens = [0.2,6]; 

% Censoring of decision times for the second-order fit (confidence)
opts.cens2 = [0,5];

% Set time granularity for the random walk
opts.dt = 1e-3; 

% Whether to plot/print fitting information
opts.doplot = 0; % this will slow the whole thing A LOT! 
opts.doprint = 0; % prints parameter values at each optimization iteration
opts.optimizationdisplay1 = 'None'; % MATLAB fminsearch display option for first-order fit
opts.optimizationdisplay2 = 'None'; % MATLAB fminsearch display option for second-order fit 

% Number of trials to simulate at each iteration
opts.N = 1000;