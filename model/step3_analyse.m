%========================================================
%   step2_fitconf.m
%
%   Reads behavioral data files and fits a race model to
%   response times, accuracy and confidence for high and low RT
%
%   Faivre, Roger et al., 2019,
%   Confidence in perceptual decision-making is preserved in schizophrenia
%
%   preregistration: https://osf.io/84wqp/
%
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.fr>
%   03/12/2019
%========================================================
clear
addpath functions
config();

opts.confmodel = 'win';
opts.confmodels = {'win','lose','balance'};
opts.level2 = 1;

% store model parameters for first-order
allparams = [];
% store model parameters for second-order
allparams2 = [];

% store log-likelihood for first-order
alllogll = [];
% store log-likelihood for second-order
alllogll2 = [];

% store simulated conf to compute m-ratio
allsuj = {};
allgrp = {};
allsimcor = [];
allsimconf = [];
allsimmvonset = [];

allsuj2 = {};
allgrp2 = {};
allcor = [];
allconf = [];
allmvonset = [];

% read data
alldata = readtable('mouse_summary.csv');
if isempty(opts.subjects)
   opts.subjects = unique(alldata.suj).';
end

s = 0;
for subj_=opts.subjects
    
    s = s+1;
    
    % separate subject and group
    subj = cell2mat(subj_);
    %sep = strfind(subj,'_');
    %id = subj((sep+1):end);
    %group{s} = subj(1:(sep-1));
    subject{s} = subj;
    
    % select data for this subject
    data = alldata(strcmp(alldata.suj,subj),:);
    group = unique(data.group);
    
    %% get first-order model parameters
    p = load(['models/' opts.type   ...
        '/s' num2str(subj) '_race_' opts.type  '.mat'],...
        'fitparams','opts','ll');
    
    % select best model
    [bestll,ibest_] = min(p.ll);
    ibest = ibest_;
    output.ll(:,:,s) = -p.ll;
    output.ibest(s) = ibest;
    
    % parameter vector
    params = [...
        p.fitparams{ibest}.bound.val, ...
        p.fitparams{ibest}.drift.val, ...
        p.fitparams{ibest}.ndtime.val, ...
        p.fitparams{ibest}.rbound.val, ...
        p.fitparams{ibest}.rho.val,...
        ];
    
    % concat
    allparams = [allparams ; params];
    alllogll = [alllogll ; -p.ll(ibest)];
    
    
    %% behavior
    
    on_ = data.mvonset;
    
    dat.onset = on_;
    dat.dir = data.dir;
    dat.rt = data.rt;
    dat.resp = data.cor;
    %dat.stimside = data.stim;
    %dat.respside = data.dir;

    dat.conf = cellfun(@str2double,data.triconf);
    dat.dir(data.stim == 1) = -dat.dir(data.stim == 1);
    
    bad =  dat.rt < opts.cens2(1) | dat.rt > opts.cens2(2);% | dat.resp < 0 | dat.conf == -1;
    
    dat.onset(bad) = [];
    dat.dir(bad) = [];
    dat.rt(bad) = [];
    dat.resp(bad) = [];
    dat.conf(bad) = [];
    
    %% simulate
    p.fitparams{ibest}.rho.val = 0;
    sim = evacc(p.fitparams{ibest},opts);
    
    % remove bad and censor
    simbad = isnan(sim.dect) | sim.rt < opts.cens2(1) | sim.dect >= opts.cens2(2);
    
    sim.dect(simbad) = [];
    sim.rt(simbad) = [];
    sim.resp(simbad) = [];
    sim.dv(simbad,:,:) = [];
    
    % store
    output.onset(s) = mean(dat.onset);
    output.onset_cor(s) = mean(dat.dir>0);
    output.onset_std(s) = std(dat.onset);
    
    output.cor(s) = mean(dat.dir>0);
    output.confcor1(:,s) = sum(dat.conf(dat.dir>0)==1)./length(dat.dir);
    output.confcor2(:,s) = sum(dat.conf(dat.dir>0)==2)./length(dat.dir);
    output.confcor3(:,s) = sum(dat.conf(dat.dir>0)==3)./length(dat.dir);
    
    output.conferr1(:,s) = sum(dat.conf(dat.dir<0)==1)./length(dat.dir);
    output.conferr2(:,s) = sum(dat.conf(dat.dir<0)==2)./length(dat.dir);
    output.conferr3(:,s) = sum(dat.conf(dat.dir<0)==3)./length(dat.dir);
    
    [hi,x] = ksdensity(dat.onset(dat.resp>0),0:0.1:10);
    output.datrtcor(:,s) = mean(dat.resp>0)*hi;
    [hi,x] = ksdensity(dat.onset(dat.resp==0),0:0.1:10);
    output.datrterr(:,s) = mean(dat.resp==0)*hi;
    [hi,x] = ksdensity(sim.rt(sim.resp>0),0:0.1:10);
    output.simrtcor(:,s) = mean(sim.resp>0)*hi;
    [hi,x] = ksdensity(sim.rt(sim.resp<0),0:0.1:10);
    output.simrterr(:,s) = mean(sim.resp<0)*hi;
    %% get second-order model parameters
    p2 = load(['models/' opts.type  ...
        '/s' num2str(subj) '_race_' opts.type '_confsim.mat'],...
        'confparams', 'sim','ll','options');
    %%
    % select decision-congruent model
    modeltype = 1;
    opts.confmodel = opts.confmodels{modeltype};
    
    % select best model (in terms of decision readout timing)
    [bestll2,ibest2] = min(p2.ll(:,modeltype));
    %ibest2 = 1;
    % store
    output.ll2(:,:,s) = -p2.ll;
    output.ibest2(s) = ibest2;
    
    % parameter vector
    params2 = [...
        p2.confparams{ibest2,modeltype}.confc1.val, ...
        p2.confparams{ibest2,modeltype}.confc2.val, ...
        p2.confparams{ibest2,modeltype}.confreadout.val, ...
        ];
    
    % concat
    allparams2 = [allparams2 ; params2];
    alllogll2 = [alllogll2 ; min(p2.ll)];
    
    % simulate confidence
    [simconf,simdvint,contconf_win,contconf_lose,contconf_bal] = dv2conf( sim.dv,sign(sim.resp)>0, sim.dect, p2.confparams{ibest2,modeltype}, opts);
    
    % store
    output.simcor(s) = mean(dat.resp>0);
    output.simconfcor1(:,s) = sum(simconf(sim.resp>0)==1)./length(sim.resp);
    output.simconfcor2(:,s) = sum(simconf(sim.resp>0)==2)./length(sim.resp);
    output.simconfcor3(:,s) = sum(simconf(sim.resp>0)==3)./length(sim.resp);
    
    output.simconferr1(:,s) = sum(simconf(sim.resp<0)==1)./length(sim.resp);
    output.simconferr2(:,s) = sum(simconf(sim.resp<0)==2)./length(sim.resp);
    output.simconferr3(:,s) = sum(simconf(sim.resp<0)==3)./length(sim.resp);
    
    
    %[simconf_,simdvint_,contconf_] = dv2conf( sim.dv,sign(sim.resp)>0, sim.dect, p2.confparams{1,modeltype}, opts);
    output.contconf1_win(:,s) = nanmean(contconf_win(simconf==1,:));
    output.contconf2_win(:,s) = nanmean(contconf_win(simconf==2,:));
    output.contconf3_win(:,s) = nanmean(contconf_win(simconf==3,:));
    output.contconfcor_win(:,s) = corr(contconf_win,simconf,'type','Spearman','rows','complete');
    
    output.contconf1_lose(:,s) = nanmean(contconf_lose(simconf==1,:));
    output.contconf2_lose(:,s) = nanmean(contconf_lose(simconf==2,:));
    output.contconf3_lose(:,s) = nanmean(contconf_lose(simconf==3,:));
    output.contconfcor_lose(:,s) = corr(contconf_lose,simconf,'type','Spearman','rows','complete');
    
    output.contconf1_bal(:,s) = nanmean(contconf_bal(simconf==1,:));
    output.contconf2_bal(:,s) = nanmean(contconf_bal(simconf==2,:));
    output.contconf3_bal(:,s) = nanmean(contconf_bal(simconf==3,:));
    output.contconfcor_bal(:,s) = corr(contconf_bal,simconf,'type','Spearman','rows','complete');
    
    
    %%
    n = length(sim.resp);
    allsuj = [allsuj(:) ; repmat(subject(s),n,1)];
    allgrp = [allgrp(:) ; repmat(group(s),n,1)];
    allsimcor = [allsimcor ; sim.resp];
    allsimconf = [allsimconf ; simconf];
    allsimmvonset = [allsimmvonset ; sim.rt];
    
    n2 = length(dat.dir);
    allsuj2 = [allsuj2(:) ; repmat(subject(s),n2,1)];
    allgrp2 = [allgrp2(:) ; repmat(group(s),n2,1)];
    allcor = [allcor ; dat.dir>0];
    allconf = [allconf ; dat.conf];
    allmvonset = [allmvonset ; dat.onset];
    
    %%
end

%% store all parameters in CSV
b = allparams(:,1);
v = allparams(:,2);

ndt = allparams(:,3);
c1 = allparams2(:,1);
c2 = allparams2(:,2);

tbl = table(subject.',group.',b,v,ndt,c1,c2,-alllogll,-mean(alllogll2,2),...
    'VariableNames',{'suj','grp','B','drift','ndt','c1','c2','logllt1','logllt2'});
writetable(tbl,'modelparams.csv');

%%
tbl = table(subject.',group.',...
    output.confcor1.',output.confcor2.',output.confcor3.',...
    output.conferr1.',output.conferr2.',output.conferr3.',...
    output.simconfcor1.',output.simconfcor2.',output.simconfcor3.',...
    output.simconferr1.',output.simconferr2.',output.simconferr3.',...
    'VariableNames',{'suj','grp','obs_conf_cor_1','obs_conf_cor_2','obs_conf_cor_3',...
    'obs_conf_err_1','obs_conf_err_2','obs_conf_err_3',...
    'sim_conf_cor_1','sim_conf_cor_2','sim_conf_cor_3',...
    'sim_conf_err_1','sim_conf_err_2','sim_conf_err_3',...
    });
writetable(tbl,'modelavgconf.csv');

%%
% store response times
t = 0:0.1:10;
data_rt_cor = output.datrtcor;
data_rt_err = output.datrterr;
sim_rt_cor = output.simrtcor;
sim_rt_err = output.simrterr;
save('modelrt.mat','data_rt_cor','data_rt_err','sim_rt_cor','sim_rt_err','t');

%%
% store confidence ratings to compute m-ratio
tbl = table(allsuj,allgrp,allsimcor>0,allsimconf,allsimmvonset,'VariableNames',{'suj','grp','simcor','simconf','simonset'});
writetable(tbl,'modelsimconf.csv');

tbl_dat = table(allsuj2,allgrp2,allcor,allconf,allmvonset,'VariableNames',{'suj','grp','cor','conf','onset'});
writetable(tbl,'modeldatconf.csv');
