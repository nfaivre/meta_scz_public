%========================================================
%   step1_fitrt.m
%
%   Reads behavioral data files and fits a race model to
%   first order choice accuracy and response times
%
%   Faivre, Roger et al., 2019, 
%   Confidence in perceptual decision-making is preserved in schizophrenia
%
%   preregistration: https://osf.io/84wqp/
%      
%   Michael Pereira <michael.pereira@univ-grenoble-alpes.fr>
%   03/12/2019
%========================================================

clear
addpath functions
config();

% load data table (copy from R-scripts directory)
alldata = readtable('mouse_summary.csv');
if isempty(opts.subjects)
   opts.subjects = unique(alldata.suj).';
end

% make directory to store model parameters
if ~exist(['models/' opts.type],'dir')
    mkdir(['models/' opts.type]);
end

s = 0;
ns = length(opts.subjects);

for subj_ = opts.subjects
    
    s = s+1;
    
    subj = cell2mat(subj_);
    
    % select data for this subject
    data = alldata(strcmp(alldata.suj,subj),:);
    
    %========================================================
    %% Process behav data 
    %========================================================
    onset = data.mvonset; % movement onset
    dir = data.dir; % direction at movement onset
    
    % we convert from stimulus-coding to accuracy-coding
    dir(data.stim == 1) = -dir(data.stim == 1);
    
    %========================================================
    %% Fitting
    %========================================================
    % load parameters
    params = paramset();
    
    % total number of init seeds
    tot = length(params.drift.init)*length(params.bound.init)*length(params.ndtime.init);
    % init params
    initparams = cell(1,tot);
    % fitted params
    fitparams = cell(1,tot);
    % final log-likelihoods
    ll = nan(1,tot);
    
    % loop across all init values
    id = 0;
    for d = 1:length(params.drift.init) % loop across initial drift rates
        for b = 1:length(params.bound.init)  % loop across initial decision bounds
            for t = 1:length(params.ndtime.init)  % loop across initial non-decision times
                id = id+1;
                %%
                initparams{id} = params;
                initparams{id}.drift.val = params.drift.init(d);
                initparams{id}.bound.val = params.bound.init(b);
                initparams{id}.ndtime.val = params.ndtime.init(t);
                
                tic;
                % for reproducibility, fix random number generator
                rng(1234);
                fprintf('Fitting %s (%d/%d) init (%d/%d): drift:%.4f, bound:%.0f, ndt: %.2f ... ',...
                    subj, s, ns, id, tot, initparams{id}.drift.val, ...
                    initparams{id}.bound.val, initparams{id}.ndtime.val);
                
                % fit movement onset and choice accuracy
                [ fitparams{id}, ll(id), out] = fitrt(onset, dir, initparams{id}, opts);
                
                t = toc;
                fprintf('[ DONE ] (ll: %.1f; %.0f s)\n',ll(id),t);
                
                %%
                if opts.doplot
                    subplot(131);
                    title(sprintf('s%d ll=%.4f',subj,ll));
                    print(['models/' opts.type '/s' num2str(subj) '_race_' opts.type '_' num2str(id) '.eps'],'-depsc');
                end
            end
        end
    end
    
    %========================================================
    %% Saving
    %========================================================
    
    save(['models/' opts.type '/s' num2str(subj) '_race_' opts.type  '.mat'],'fitparams','initparams','opts','ll');
    
end