Data and analysis files corresponding to the two following articles: 

+ Faivre, N.*, Roger, M.*, Pereira, M., de Gardelle, V., Vergnaud, JC., Passerieux, C., and Roux, P. (2020). Confidence in perceptual decision-making is preserved in schizophrenia. Journal of Psychiatry and Neuroscience, doi: 10.1503/jpn.200022 

+ Rouy, M., Roger, M., Goueytes, D., Pereira, M., Roux, P., and Faivre, N. (2023). Preserved electrophysiological markers of confidence in schizophrenia spectrum disorder. Schizophrenia, doi: 10.1038/s41537-023-00333-4

Comparison of metacognitive performance between individuals with schizophrenia and matched controls during visual motion discrimination task.  

Preregistration was submitted to https://osf.io/84wqp but the link was erased accidentally. The same file can be found on this repository (./Preregitration_final.docx) or is available here: https://clinicaltrials.gov/show/NCT03140475

**Content** 

data: where behavioral and eeg anonymized data are donwloaded and models are saved

figures: where final figures are saved

model: matlab scripts for bounded evidence accumulation model

behav: R scripts for all behavioral analyses (use analysis_master.Rmd, make sure all libs in ./functions/loadlibs.R are installed)

eeg: matlab and R scripts for all eeg analyses

