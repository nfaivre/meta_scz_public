function [cor_coef, P, EEG, screen] = check_RTs(EEG,behavroot,sujnumber)

counter = 0;
type = [];
for i = 1:length(EEG.event)
    if (EEG.event(i).type > 100)
        counter = counter + 1;
        type = [type EEG.event(i).type];
    end
    EEG.event(i).trial_tmp = counter;
    EEG.event(i).trial = 0;
end

% figure()
% plot(type)
trial = unique([EEG.event.trial_tmp]);
rt = [];
id12 = [];
id14 = [];
for i = trial
    idx  = find([EEG.event.trial_tmp] == i);
    id12 = idx([EEG.event(idx).type] == 12);
    id14 = idx([EEG.event(idx).type] == 14);
    if (~(isempty(id12) || isempty(id14)))
        id12= id12(1);
        dif  = EEG.event(id14).latency - EEG.event(id12).latency;
        rt = [rt dif];
    end 
end
rt = rt/256;
% Load processed behavioral files
behavfile=dir([behavroot '/data_suj' num2str(sujnumber) '.mat']);
load([behavfile.folder filesep behavfile.name]);

% reshape 10*30 matrix into 1*300 vector
P.cor = reshape(P.cor',1,prod(size(P.cor)));
P.RT = reshape(P.RT',1,prod(size(P.cor)));
P.rdk.sigref = reshape(P.rdk.sigref',1,prod(size(P.cor)));
P.design.stims = reshape(P.design.stims',1,prod(size(P.cor)));
P.clickside = reshape(P.clickside',1,prod(size(P.cor)));
P.VAS.confidence = reshape(P.VAS.confidence',1,prod(size(P.cor)));
P.blocktype = reshape(P.blocktype',1,prod(size(P.cor)));
P.VAS.movRT = reshape(P.VAS.movRT',1,prod(size(P.cor)));
P.VAS.firstRT = reshape(P.VAS.firstRT',1,prod(size(P.cor)));

RT = [P.RT];
if sujnumber == 16  
    RT = RT((300-length(rt)+1):end); % EEG Recordings begins at trial 34
end

figure()
subplot(2,2,1);
histogram(RT,30)
xlabel("Response times (from behavior)")
ylabel("Number of events")
subplot(2,2,3);
histogram(rt,30)
xlabel("Response times (from triggers)")
ylabel("Number of events")
subplot(2,2,[2,4]);
plot(RT,rt);
xlabel("Response times (from behavior)")
ylabel("Response times (from triggers)")

cor_coef = corr(RT',rt');

end
