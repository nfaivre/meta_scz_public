function P = compute_vmax(P,screen)

dt = 1/60; % mouse sampling frequency in Hz
sgwin = 3; % filter width

[b,g] = sgolay(2,sgwin*2+1);

[nb,nt] = size(P.stimonset);

method = 'sgolay';
%% loop through blocs and trials
for b = 1:nb
    for t = 1:nt
        if strcmp(method,'sgolay')
            % position
            i = (b-1) * nt + t;
            pos{i} = sgolayfilt(P.Xs(b,t).X,2,sgwin*2+1) + 1i*sgolayfilt(P.Ys(b,t).Y,2,sgwin*2+1);
            % derivate velocity
            vel{i} = filter(g(:,2),1,-1/dt*pos{i});
            vel{i}(1:sgwin) = [];
            vel{i}(1:sgwin) = 0;
            vel{i}(end+(-1:3)) = 0;
        else
            pos{i} = P.Xs(b,t).X + 1i*P.Ys(b,t).Y;
            vel{i} = diff(pos{t});
        end
    end
end

% x = [];
% vmax = [];
% for t =1:5
%     vmax(t) = max(abs(vel{t})) * screen.dotPitch/1000 * 60; % v (m.s-1)
%     x(t) = find(abs(vel{t}) == max(abs(vel{t})));
% end
% figure()
% plot(abs(vel{1})* screen.dotPitch/1000 * 60) 
% hold on 
% plot(abs(vel{2})* screen.dotPitch/1000 * 60)
% hold on  
% plot(abs(vel{3})* screen.dotPitch/1000 * 60)
% hold on 
% plot(abs(vel{4})* screen.dotPitch/1000 * 60)
% hold on 
% plot(abs(vel{5})* screen.dotPitch/1000 * 60)
% hold on
% scatter(x,vmax)


for t =1:nb*nt
    vmax = max(abs(vel{t}))* screen.dotPitch/1000 * 60;
    if (isempty(vmax) || vmax < 0)
        vmax = 0;
    end
    P.vmax(t) = vmax;
end
end