function P = recompute_movonset(P, screen)

dt = 1/60; % mouse sampling frequency in Hz
sgwin = 3; % filter width

[b,g] = sgolay(2,sgwin*2+1);

velthr = 0.2; % velocity threshold to detect movement onset

[nb,nt] = size(P.stimonset);

% set the starting threshold on vertical position at 25% of full distance
ythr = P.design.initclick(2)-1/4*(P.design.initclick(2)-P.design.resprectL(4));

method = 'sgolay';
%% loop through blocs and trials
for b = 1:nb
    for t = 1:nt
        if strcmp(method,'sgolay')
            % position
            i = (b-1) * nt + t;
            pos{i} = sgolayfilt(P.Xs(b,t).X,2,sgwin*2+1) + 1i*sgolayfilt(P.Ys(b,t).Y,2,sgwin*2+1);
            % velocity
            vel{i} = filter(g(:,2),1,-1/dt*pos{i});
            vel{i}(1:sgwin) = [];
            vel{i}(1:sgwin) = 0;
            vel{i}(end+(-1:3)) = 0;
        else
            pos{i} = P.Xs(b,t).X + 1i*P.Ys(b,t).Y;
            vel{i} = diff(pos{t});
        end
    end
end


for t =1:nb*nt
    lowsp = abs(vel{t})./max(abs(vel{t}));
    % last time under velocity threshold but haven't passed 20% of
    % full distance
    nextzero = find(lowsp > velthr,1,'first') - 5;
    if (isempty(nextzero) || nextzero < 0)
        nextzero = 0;
    end
    P.movRT(t) = nextzero/60;
    
    % Compute Vmax
     vmax = max(abs(vel{t}))* screen.dotPitch/1000 * 60;
    if (isempty(vmax) || vmax < 0)
        vmax = 0;
    end
    P.vmax(t) = vmax;
end
end