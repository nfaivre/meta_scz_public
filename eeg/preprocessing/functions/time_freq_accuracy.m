clear;
close all;

addpath(genpath([pwd '\functions']));
addpath 'C:\Users\Martin\Documents\GitHub\meta_scz\scripts\eeg\functions'
addpath('C:/Users/Martin/Desktop/Setup/eeglab2021.0')
addpath('C:/Users/Martin/Desktop/Setup/eeglab2021.0/gtecimport')
eegroot =   'C:/Users/Martin/Documents/GitHub/meta_scz/data/eeg/';
if ~exist('DEFAULT_COLORMAP'), DEFAULT_COLORMAP = 'jet(256)'; end; % Default colormap

[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;

do_preproc = 0;
%% Loop on groups and participants
if do_preproc
    for gp = 1:2
        if gp == 1
            group = 'patients';
        else
            group = 'controls';
        end
        eegrootproc =  ['C:/Users/Martin/Documents/GitHub/meta_scz/data/eeg/procdata/movelocked/' group] ;
        cd([eegrootproc]);
        % list all subjects
        allSubs = dir ([eegroot 'rawdata/' group '/R*']);

        %% define subject
        sub_patients = [1,2,4:11,14,17:19];
        sub_controls = [1:3,5:15,18:22];
        
        if group == "patients"
            subs = sub_patients;
        else
            subs = sub_controls;
        end
        len = length(subs);

        % Loop on participants
        count = 1;
        for isub = subs 
            sujfile = allSubs(isub).name;
            if group == 'controls'
                sujnumber = str2double(sujfile(16:17));
            elseif group == 'patients'
                sujnumber = str2double(sujfile(23:24));
            end
            sujname = ['S' num2str(sujnumber)];

            path2data = [eegroot '/rawdata/' group '/' sujfile];
            inputfilename = dir(path2data);
            outfold_mov  = [eegroot 'procdata/movelocked/' group];
            outputfilename = sprintf('%s_all_ICA_clean.set',sujname);

            EEG = pop_loadset([outfold_mov '/' outputfilename]);

            dataInc = pop_selectevent(EEG, 'cor', 0);
            dataCor = pop_selectevent(EEG, 'cor', 1);
            elec = 30;
            
            [Pac0, itc1, powbase1, times1, freqs1]= pop_newtimef(dataInc, 1, elec, [-500 1000], [3 0.5], 'freqs',[4 45], 'padratio', 16, ...
                'plotphase', 'off', 'plotersp','off', 'plotitc','off'); 
            [Pac1, itc2, powbase2, times2, freqs2]= pop_newtimef(dataCor, 1, elec, [-500 1000], [3 0.5], 'freqs',[4 45], 'padratio', 16, ...
                'plotphase', 'off', 'plotersp','off', 'plotitc','off'); % ,'alpha', .05, 'mcorrect', 'fdr', 'maxfreq', 45
            
            if count == 1  % create empty arrays if first participant
                allersp = zeros([ size(Pac0) len]);
            end
            allersp (:,:,count) = Pac0 - Pac1;
            count = count+1;
            clear EEG
        end
        
        cd(eegroot);
        if group == 'patients'
            ersp_patients = mean(allersp,3);
            save('mean_cor_patients', 'ersp_patients');
            save('all_cor_patients', 'allersp');
            all_patients = allersp;
        else 
            ersp_controls = mean(allersp,3);
            save('mean_cor_controls', 'ersp_controls');
            save('all_cor_controls', 'allersp');
            all_controls = allersp;
        end
    end
    save('vars', 'times1', 'freqs1');
else
    cd(eegroot);
    load('mean_cor_patients.mat')
    load('mean_cor_controls.mat')
    load('all_cor_patients.mat')
    all_patients = allersp;
    load('all_cor_controls.mat')
    all_controls = allersp;
    load('vars.mat');
end

%% Plot
figure
subplot(2,1,1);
imagesc(times1, freqs1, ersp_controls);
hcb = colorbar;
caxis([-0.4, 0.8]);
title([ 'Controls: Accuracy Correct vs Incorrect' ]);
set(gca,'ydir','normal');  % make frequency ascend or descend
ylabel('Frequency (Hz)');
title(hcb,'dB')

subplot(2,1,2); 
imagesc(times1, freqs1, ersp_patients);
colorbar;
title([ 'Patients: Accuracy Correct vs Incorrect' ]);
set(gca,'ydir','normal');  % make frequency ascend or descend
title(hcb,'dB')

colormap(DEFAULT_COLORMAP)
xlabel('Time (ms)');
ylabel('Frequency (Hz)');

cd(eegroot)
print(['Time-Frequency_cor.png'],'-dpng');

%% Stats

[h,p,~,stats] = ttest2(all_patients, all_controls, 'dim',3,'tail', 'both');
[fdr, crit_p,~, adj_p] = fdr_bh(p); % fdr-correction for multiple comparisons
% p(p>0.05) = NaN;
imAlpha=ones(size(p));
imAlpha(isnan(p))=0;

pcorrected = adj_p;
pcorrected(pcorrected > 0.05) = NaN;
imAlpha2 = ones(size(pcorrected));
imAlpha2(isnan(pcorrected))=0;

figure
imagesc(times1, freqs1, p, 'AlphaData', imAlpha);
% xline(0,'r', 'LineStyle', '--')
rectangle('Position',[0, 0, 150, 45], 'EdgeColor', 'r', 'LineStyle', '--');
hcb=colorbar;
title([ 'Correctness: Patients - Controls. p < .05' ]);
set(gca,'ydir','normal');  % make frequency ascend or descend
set(gca,'color',0.8*[1 1 1]);
xlabel('Time (ms)');
ylabel('Frequency (Hz)');
title(hcb,'p-value')

cd(eegroot)
print(['Stats_Time-Frequency_cor.png'],'-dpng');

%% Plot both
figure
% subplot(2,1,1);
imagesc(times1, freqs1, adj_p, 'AlphaData', imAlpha);
rectangle('Position',[10, 0, 320, 45], 'EdgeColor', 'r', 'LineStyle', '--');
hcb = colorbar;
title([ 'Correctness: Patients - Controls' ]);
set(gca,'ydir','normal'); % make frequency ascend or descend
set(gca,'color',0.8*[1 1 1]);
xlabel('Time (ms)');
ylabel('Frequency (Hz)');
title(hcb,'Adj. p-value')

% subplot(2,1,2);
% imagesc(times1, freqs1, pcorrected, 'AlphaData', imAlpha2);
% % xline(0,'r', 'LineStyle', '--')
% rectangle('Position',[10, 0, 320, 45], 'EdgeColor', 'r', 'LineStyle', '--');
% hcb = colorbar;
% title([ 'Correctness: Patients - Controls. p < .05' ]);
% set(gca,'ydir','normal');  % make frequency ascend or descend
% set(gca,'color',0.8*[1 1 1]);
% caxis([0.2, 1]);
% xlabel('Time (ms)');
% ylabel('Frequency (Hz)');
% title(hcb,'Adj. p-value');

cd(eegroot)
print(['Stats_adjusted_Time-Frequency_cor2.png'],'-dpng');

%%%%%%%%%%%%%%%%% Plot all
figure
subplot(2,2,1);
imagesc(times1, freqs1, ersp_controls);
hcb = colorbar;
caxis([-0.4, 0.8]);
title([ 'Controls: Cor. vs Incor.' ]);
set(gca,'ydir','normal');  % make frequency ascend or descend
ylabel('Frequency (Hz)');
title(hcb,'dB')
colormap(gca,'jet')

subplot(2,2,2); 
imagesc(times1, freqs1, ersp_patients);
hcb = colorbar;
title([ 'Patients: Cor. vs Incor.' ]);
set(gca,'ydir','normal');  % make frequency ascend or descend
ylabel('Frequency (Hz)');
title(hcb,'dB')
colormap(gca,'jet')

subplot(2,2,[3,4]);
imagesc(times1, freqs1, adj_p, 'AlphaData', imAlpha);
rectangle('Position',[10, 0, 320, 45], 'EdgeColor', 'r', 'LineStyle', '--');
title([ 'Correctness: Patients - Controls' ]);
set(gca,'ydir','normal'); % make frequency ascend or descend
% set(gca,'color',0.8*[1 1 1]);
caxis([0.01, 0.5]);
xlabel('Time (ms)');
ylabel('Frequency (Hz)');
cmap = flipud(hot);
colormap(gca,cmap)
hcb = colorbar;
title(hcb,'Adj. p-value')

xlabel('Time (ms)');
ylabel('Frequency (Hz)');

cd(eegroot)
print(['Time-Frequency_cor_all.png'],'-dpng');
