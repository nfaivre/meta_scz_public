clear;
close all;

group     = 'controls';
if group == 'patients'
    gp = 1;
else
    gp = 0;
end

addpath(genpath([pwd '\functions']));
addpath 'C:\Users\Martin\Documents\GitHub\meta_scz\scripts\eeg\functions'
addpath('C:/Users/Martin/Desktop/Setup/eeglab2021.0')
addpath('C:/Users/Martin/Desktop/Setup/eeglab2021.0/gtecimport')
eegroot =   'C:/Users/Martin/Documents/GitHub/meta_scz/data/eeg';
file_infos = 'C:\Users\Martin\Documents\GitHub\meta_scz\data\eeg\procdata\movelocked\infos.xlsx';

[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;

cd([eegroot]);

% list all subjects
allSubs = dir ([eegroot '/rawdata/' group '/R*']);
%%
for isub = 1:24
    %% define subject
    sujfile = allSubs(isub).name;
    if group == 'controls'
        sujnumber = str2double(sujfile(16:17));
    elseif group == 'patients'
        sujnumber = str2double(sujfile(23:24));
    end
    sujname = ['S' num2str(sujnumber)];

    % path2data = [eegroot '/rawdata/' group '/' sujfile];
    % inputfilename = dir(path2data);

    table = readtable(file_infos);
    i = find(table.num == sujnumber & table.gp == gp);

    %% SAVE
    infold = [eegroot '/procdata/movelocked/' group];
%     infold = [eegroot '/procdata/movelocked/bids'];
    cd(infold);
    inputfilename = sprintf('%s_all_ICA_clean.set',sujname);
%     inputfilename = 'suj44_all_ICA_clean.set';
    EEG = pop_loadset([infold '/' inputfilename]);

    outputfilename = sprintf('suj%i_all_ICA_clean.set',i);
    EEG.filename = outputfilename;
    EEG.datfile = sprintf('suj%i_all_ICA_clean.fdt',i);
    outfold  = [eegroot '/procdata/movelocked/bids'];
    EEG = pop_saveset(EEG,'filename',outputfilename,'filepath',outfold);

    clear EEG
end
