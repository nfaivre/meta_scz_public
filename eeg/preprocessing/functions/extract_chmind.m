function [EEG, ch_mind, rt_chmind, flag_chmind] = extract_chmind(EEG, P) 
dt = 1/60;
sgwin = 5;
velthr = 0.2; % velocity threshold to detect movement onset

[b,g] = sgolay(2,sgwin*2+1);
method = 'sgolay';
ythr = P.design.initclick(2)-1/3*(P.design.initclick(2)-P.design.resprectL(4)); 
[nb,nt] = size(P.stimonset);
% set the right limit of the left target
llim = P.design.resprectL(3);
% set the left limit of the right target
rlim = P.design.resprectR(1);

clear pos vel acc
%% loop through trials
for x=1:nb
    for y = 1:nt 
        i = (x-1)*nt+y;
        if strcmp(method,'sgolay')
            % position
            pos{i} = sgolayfilt(P.Xs(x,y).X,2,sgwin) + 1i*sgolayfilt(P.Ys(x,y).Y,2,sgwin);
            
            vel{i} = filter(g(:,2),1,-1/dt*pos{i});
            vel{i}(1:sgwin) = [];
            vel{i}(1:sgwin) = 0;
            vel{i}(end+(-1:3)) = 0;
            
        end
    end
end

%%
clear dir

chmind = false(1,nb*nt);
rt_chmind = -ones(1,nb*nt);
flag_chmind = zeros(1,nb*nt);
bad_trials = zeros(1,nb*nt);

fig = figure();
fig.Units='normalized';
fig.OuterPosition=[0 0 1 1];

for t=1:nb*nt
    
    %% Computing move onset to be coherent with recomputed move onset
    lowsp = abs(vel{t})./max(abs(vel{t}));
    % last time under velocity threshold but haven't passed 25% of
    % full distance
    nextzero = find(lowsp > velthr,1,'first') - 5;
    if (isempty(nextzero) || nextzero < 0)
        nextzero = 0;
    end

    
    %% Finding the position relative to bissect the first time ythresh is crossed
    bissect2 = (rlim+llim)/2;
    idx = find(imag(pos{t}) <= ythr);
    %idx = find(imag(pos{t}) <= nextzero);
    if ~isempty(idx)
        x_pos = real(pos{t});
        sign_pos = x_pos(idx(1):end)-bissect2;
        test = find(diff(sign(sign_pos)));  
    else
        test = [];
    end
   
    if ~isempty(test)
        chmind(t) = 1;
        flag_chmind(t) = 1;
        
        subplot(1,2,1);
        h = plot(pos{t});
        ylim([170 650]);
        xline(bissect2);
        xlim([P.design.resprectLmid(1)-150 P.design.resprectRmid(1)+150]);
        title([P.suj.number ' Trial ' int2str(t) ': Trajectory'])
        
        %left target circle
        radius = (P.design.resprectL(3)-P.design.resprectL(1))/2;
        center_coord = P.design.resprectLmid;
        centerX = center_coord(1);
        centerY = center_coord(2);
        viscircles([centerX, centerY], radius, 'EdgeColor','b');
        axis square;
        
        %Right target circle
        radius = (P.design.resprectR(3)-P.design.resprectR(1))/2;
        center_coord = P.design.resprectRmid;
        centerX = center_coord(1);
        centerY = center_coord(2);
        viscircles([centerX, centerY], radius);
        axis square;
        
        %Initial position circle
        radius = (P.design.resprectR(3)-P.design.resprectR(1))/2;
        coord_init = P.design.initclick;
        centerX = bissect2;
        centerY = (coord_init(2)+coord_init(4))/2;
        viscircles([centerX, centerY], radius, 'EdgeColor','k');
        axis square;
        
        yline(ythr);
        ytest = imag(pos{t});
        yline(ytest(nextzero), 'r');
        button = 0;
        reselect_vel = 0;
        
        subplot(1,2,2)
        g = plot(abs(vel{t}));
        xline(nextzero,'r');
        title([P.suj.number ' Trial ' int2str(t) ' Velocity profile'])
        
        axes = get(fig,'children');
        
        while true
            if button == 0
                subplot(1,2,1)
                legend = text(bissect2,620,['Left Mouse: select chmind, Middle Mouse: Remove Trial,' newline 'Right Mouse: Not Chmind'],'HorizontalAlignment','center','Color', [0 0 0], 'FontSize',14);
            end
            [test_x,test_y, input1] = ginput(1);
            if input1 == 1
                delete(legend);
                subplot(1,2,1)
                legend = text(bissect2,620,['Left Mouse: Reselect,' newline 'Right click: Next trial'],'HorizontalAlignment','center','Color', [0 0 0], 'FontSize',14);
                hx = h.XData; 
                hy = h.YData; 
                dh = sqrt((test_x-hx).^2 + (test_y-hy).^2); 
                [~,Idx] = min(dh);
                h1 = text(hx(Idx),hy(Idx),'O','HorizontalAlignment','center','Color', [1 0 0], 'FontSize',12);               
                
                if ismember(Idx,[326,327])
                    Idx = 325;
                end
                
                gx = g.XData; 
                gy = g.YData; 
                subplot(1,2,2)
                g1 = text(gx(Idx),gy(Idx),'O','HorizontalAlignment','center','Color', [1 0 0], 'FontSize',12);
                legend_vel = text((min(xlim)+max(xlim)/2), max(ylim)-70,'Left Mouse: Select velocity timing','HorizontalAlignment','center','Color', [0 0 0], 'FontSize',14);
                
                if reselect_vel ~= 1
                    [test_x, test_y, input2] = ginput(1);
                    axnum = find(ismember(axes, gca));
                end
                delete(legend);
                
                if input2 == 1                 
                    if axnum == 2                     
                       delete(g1);
                       delete(h1);
                       delete(legend_vel);
                    else
                        subplot(1,2,2)
                        legend = text(bissect2,620,'Left Mouse: Reselect, Middle Mouse: flag, Right Mouse: Next trial','HorizontalAlignment','center','Color', [0 0 0], 'FontSize',14);
                        d = abs(test_x-gx); 
                        [~,minIdx] = min(d);
                        x_index = gx(minIdx);
                        y_index = gy(minIdx);
                        g2 = text(gx(minIdx),gy(minIdx),'O','HorizontalAlignment','center','Color', [0 1 0], 'FontSize',12);                
                        subplot(1,2,1)
                        h2 = text(hx(minIdx),hy(minIdx),'O','HorizontalAlignment','center','Color', [0 1 0], 'FontSize',12);
                        
                        [~,~,button] = ginput(1);
                        delete(legend);
                        if button == 1
                            delete(g2);
                            delete(h2);
                            delete(legend_vel);
                            reselect_vel = 1;
                        elseif button == 3
                            break;                    
                        elseif button == 2
                            flag_chmind(t) = 2;
                            delete(legend);
                            break;
                        end  
                    end
                elseif input2 == 3
                    break;
                end               
            elseif input1 == 2
                x_index = 0;
                bad_trials(t) = t;
                break
            elseif input1 == 3
                x_index = 0;
                chmind(t) = 0;
                break
            end
        end
        
        %timing_chmind
        timing_chmind = x_index*dt;
        
        check = size(timing_chmind); % not a great fix for the case where click returns ambiguous data but better than nothing
        if check(2)>1
            timing_chmind = timing_chmind(1);            
        end
        rt_chmind(t) = timing_chmind;    
    else
        chmind(t) = 0;
    end
    
    
end

rm_trial = bad_trials(:).';
rm_trial = rm_trial';

ch_mind = chmind(:).';
ch_mind= ch_mind';

rt_chmind = rt_chmind(:).';
rt_chmind = rt_chmind';

flag_chmind = flag_chmind(:).';
flag_chmind = flag_chmind';

for k =  1:length(chmind) %loop on trials
    for e = find([EEG.event.trial] == k)
        EEG.event(e).ch_mind    = ch_mind(k);
        EEG.event(e).ch_mind_rt = rt_chmind(k);
        EEG.event(e).rm_trial   = rm_trial(k);
        EEG.event(e).flag_chm   = flag_chmind(k);
    end
end

delete(fig)

end