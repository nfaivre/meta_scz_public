clear;
close all;

group     = 'controls';
if group == 'patients'
    gp = 1;
else
    gp = 0;
end

addpath(genpath([pwd '\functions']));
addpath 'C:\Users\Martin\Documents\GitHub\meta_scz\scripts\eeg\functions'
addpath('C:/Users/Martin/Desktop/Setup/eeglab2021.0')
addpath('C:/Users/Martin/Desktop/Setup/eeglab2021.0/gtecimport')
eegroot =   'C:/Users/Martin/Documents/GitHub/meta_scz/data/eeg';
file_infos = 'C:\Users\Martin\Documents\GitHub\meta_scz\data\eeg\procdata\movelocked\infos.xlsx';
cd([eegroot]);

% list all subjects
allSubs = dir ([eegroot '/rawdata/' group '/R*']);
if gp == 1
    subs = [1,2,4:11,13,14,16:20];
else
    subs = 1:24;
end
%%
for isub = subs
    %% define subject
    sujfile = allSubs(isub).name;
    if group == 'controls'
        sujnumber = str2double(sujfile(16:17));
    elseif group == 'patients'
        sujnumber = str2double(sujfile(23:24));
    end
    sujname = ['S' num2str(sujnumber)];

    path2data = [eegroot '/rawdata/' group '/' sujfile];
    inputfilename = dir(path2data);
    outfold  = [eegroot '/rawdata/setfiles'];

%% LOAD THE DATA (inlcuding merging several files if any)
[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;
for file=1:length(inputfilename)
    EEGtmp = pop_read_gtec(ALLEEG, path2data);
    [EEG, EEGtmp] = eeg_store(EEG,EEGtmp);
end
if file>1,EEG = pop_mergeset(EEG(1),EEG(2));end

%% SAVE
cd(outfold);
table = readtable(file_infos);
i = find(table.num == sujnumber & table.gp == gp);
sujfile = sprintf('suj%i_rawdata', i);
pop_saveset(EEG,'filename',sujfile,'filepath',outfold);
clear EEG
end

