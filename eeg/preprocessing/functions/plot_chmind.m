dt = 1/60;
sgwin = 5;


[b,g] = sgolay(2,sgwin);
method = 'sgolay';
ythr = P.design.initclick(2)-1/3*(P.design.initclick(2)-P.design.resprectL(4)); 
[nb,nt] = size(P.stimonset);
% set the right limit of the left target
llim = P.design.resprectL(3);
% set the left limit of the right target
rlim = P.design.resprectR(1);

clear pos vel acc
%% loop through trials
for x=1:nb
    for y = 1:nt 
        i = (x-1)*nt+y;
        if strcmp(method,'sgolay');
            % position
            pos{i} = sgolayfilt(P.Xs(x,y).X,2,sgwin) + 1i*sgolayfilt(P.Ys(x,y).Y,2,sgwin);
            
        end
    end
end

index = find(ch_mind).';
index2 = 1:300;
index2 = setdiff(index2,index);

figure();
hold on 
for i = index
    plot(pos{i});
end
axis([400 900 200 600])
axis equal
figure();
hold on 
for i = index2
    plot(pos{i});
end
axis([400 900 200 600])
axis equal