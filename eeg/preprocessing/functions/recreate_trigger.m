movonset = [P.movRT];

ev = find([EEG.event.type] == trigM1); % find all triggers corresponding to mouse onset
diflat =  [diff([EEG.event(ev).latency]) 1001]; % compute ITI
goodtrials = find(diflat > 500); % remove triggers with too short ITI (sometimes triggers are played twice by mistake)
goodtrials = ev(goodtrials);

id12 = [];
id13 = [];
preMov = [];
postMov = [];
stimonset = [];
for i = 1:length(goodtrials)
    trial_tmp = EEG.event(goodtrials(i)).trial_tmp;
    idx  = find([EEG.event.trial_tmp] == trial_tmp);
    for ii = idx
        EEG.event(ii).trial = i;
    end
    if (i < length(goodtrials))
        idx_next  = find([EEG.event.trial_tmp] == trial_tmp + 1);
    end
    id12 = idx([EEG.event(idx).type] == trigstim);
    id13 = idx([EEG.event(idx).type] == trigM1);

    if (~isempty(id13))
        id13 = id13(1);
    end 

    if (~(isempty(id12) || isempty(id13)))
        EEG.oldevent(i).latency = EEG.event(id13).latency;
        EEG.oldevent(i).type = trigM1;
        EEG.oldevent(i).trial = i;
        EEG.event(id13).latency = EEG.event(id12).latency + ((P.movRT(i)+ 0.3)*EEG.srate); % in sample unit
        
        %Flag rejected trials here
        if P.movRT(i) <= 0.1
            P.keepM1(i) = 0;
            for ii = idx 
                EEG.event(ii).keepM1 = 0; 
            end
        else
            P.keepM1(i) = 1;
            for ii = idx 
                EEG.event(ii).keepM1 = 1; 
            end
        end
        
        id100 = idx([EEG.event(idx).type] > 100);
        trial = EEG.event(id100).type;
        trial_onset = EEG.event(id100).latency;
        
        latency_onset = (EEG.event(id12).latency - trial_onset)/EEG.srate; % in sec
        stimonset = [stimonset latency_onset];
        
        latency_preMov = (EEG.event(id13).latency - trial_onset)/EEG.srate; % in sec
%         if (latency_preMov < 1)
%             P.shortMov(i) = 1;
%             for ii = idx
%                 EEG.event(ii).shortMov = 1; 
%             end
%         else
%             P.shortMov(i) = 0;
%             for ii = idx
%                 EEG.event(ii).shortMov = 0; 
%             end
%         end
            
        if (i < length(goodtrials))
            id101 = idx_next([EEG.event(idx_next).type] > 100);            
            trialNext_onset = EEG.event(id101).latency; % in sample unit
            latency_postMov = (trialNext_onset - EEG.event(id13).latency)/EEG.srate; % in sec        
        else 
            id17 =  idx([EEG.event(idx).type] == 17);
            trigR2 = EEG.event(id17).latency; % in sample unit
            latency_postMov = (trigR2 - EEG.event(id13).latency)/EEG.srate; % in sec
        end
        
        preMov  = [preMov latency_preMov];    % in sec
        postMov = [postMov latency_postMov];  % in sec 
    end 
end

%% Delete rows with duplicate triggers
id = find([EEG.event.trial] == 0);
for i = id
    EEG.event(i) = [];
end

%% Merge behavioral data in EEG struct:

for k =  1:prod(size(P.cor)) %loop on trials
    for e = find([EEG.event.trial] == k)
        EEG.event(e).cor    = P.cor(k);
        EEG.event(e).RT     = P.RT(k);
        EEG.event(e).signal = P.rdk.sigref(k);
        EEG.event(e).dir    = P.design.stims(k);
        EEG.event(e).click  = P.clickside(k);
        EEG.event(e).conf   = P.VAS.confidence(k);
        EEG.event(e).cond   = P.blocktype{k};
        EEG.event(e).RT2    = P.VAS.movRT(k);
        EEG.event(e).RT2start = P.VAS.firstRT(k);
    end
end
