function eegplugin_autorejICA(fig,try_strings,catch_strings)


toolsmenu = findobj(fig,'Tag','tools');
abovemenupos = get(findobj(toolsmenu,'Label','Remove components'),'position');


uimenu( toolsmenu,'position',abovemenupos+1,'separator','on', 'label', 'Automatic ICA components selection', 'callback',...
    [ 'pop_autorejICA(EEG);' ]); 