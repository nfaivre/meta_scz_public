clear;
close all;

isub      = 1; % choose participant number to analyse
% doplot    = 0; % plot raw data
dorunica  = 1; % run ica

root = 'C:\Users\Martin\Documents\GitHub\meta_scz_public\eeg';
addpath(genpath([root filesep 'preprocessing' filesep 'functions']));

% load eeglab
addpath('C:/Users/Martin/Desktop/Setup/eeglab2021.0')
addpath('C:/Users/Martin/Desktop/Setup/eeglab2021.0/gtecimport')
icatype = 'runica'; % ICA type

eegroot =    'C:/Users/Martin/Documents/GitHub/meta_scz_public/data/eeg';
behavroot = ['C:/Users/Martin/Documents/GitHub/meta_scz_public/data/eeg/behav/'];
file_badChans = [eegroot filesep 'meta_scz_badchannels.xlsx'];


[ALLEEG EEG CURRENTSET ALLCOM] = eeglab;
cd([eegroot]);

path2files = [eegroot '/ds004367/sub-S*/eeg/' ];
allSubs = dir(fullfile(path2files, '*.set'));
allfdt = dir(fullfile(path2files, '*.fdt'));

%% define subject
if isub > 23
   group = 'patient';
else
   group = 'control'; 
end

sujfile = allSubs(isub).name;
sujfolder = sujfile(1:7);
sujnumber = str2num(sujfile(6:7));
path2data = [eegroot '/ds004367/' sujfolder '/eeg/'];
path2set = [path2data '/' sujfile];
path2fdt = [path2data '/' allfdt(isub).name];
inputfilename = dir(path2set);

outfold  = [eegroot '/procdata/'];

%% Create output directory if necessary.
if ~isfolder(outfold)
    mkdir(outfold);
end

%% LOAD THE DATA
cd(path2data);
EEG = pop_loadset(path2set);

goodlabels={'FP1', 'FPz', 'FP2', 'AF7', 'AF3' 'AF4', 'AF8', 'F7', 'F5', 'F3', 'F1', 'Fz', 'F2', 'F4', 'F6', 'F8', 'FT7',...
    'FC5', 'FC3', 'FC1', 'FCz', 'FC2', 'FC4', 'FC6', 'FT8', 'T7', 'C5', 'C3', 'C1', 'Cz', 'C2', 'C4', 'C6', ...
    'T8', 'TP7', 'CP5', 'CP3', 'CP1', 'CPz', 'CP2', 'CP4', 'CP6', 'TP8', 'P7', 'P5', 'P3', 'P1', 'Pz', 'P2',...
    'P4', 'P6', 'P8', 'PO7', 'PO3', 'POz','PO4', 'PO8', 'O1', 'Oz', 'O2', 'O9', 'O10', 'AFz','A2'...
    'GSR1', 'GSR2', 'ECG1', 'ECG2'}; 
%% switch AFz & TP7 from patient 10, as we had bad connection
if strcmp(group,'patient') &&  (sujnumber >= 4 || sujnumber < 2)
    tp7 = find(ismember(goodlabels,'TP7'));afz = find(ismember(goodlabels,'AFz'));
    goodlabels{tp7}='AFz';    goodlabels{afz}='TP7';
end															 																 																													

%% switch AFz & TP7 from control 8, as we had bad connection
if strcmp(group,'control') &&  sujnumber > 7 && sujnumber < 14
    tp7 = find(ismember(goodlabels,'TP7'));afz = find(ismember(goodlabels,'AFz'));
    goodlabels{tp7}='AFz';    goodlabels{afz}='TP7';
end

%% switch AFz & TP8 from control 14, as we had bad connection
if strcmp(group,'control') && sujnumber > 13
    tp8=find(ismember(goodlabels,'TP8'));afz=find(ismember(goodlabels,'AFz')); tp7=find(ismember(goodlabels,'TP7'));
    goodlabels{afz}='TP7'; goodlabels{tp8}='AFz'; goodlabels{tp7}='TP8';
end

%% correct for wrong labeling of FT7 (mistakenly labeled F7) for controls 1 to 15
if strcmp(group,'control') &&  sujnumber < 16
    EEG.chanlocs(17).labels = 'FT7;';
end

for e=1:size(EEG.chanlocs,2)
    EEG.chanlocs(e).labels = goodlabels{e};
end

%% load channel location file
cd(root);
EEG = pop_chanedit(EEG,'lookup');
cd([eegroot]);


%% remove channel
% auxiliary
EEG = pop_select(EEG,'nochannel',find(ismember({EEG.chanlocs.labels},{'A2','GSR1','GSR2','ECG1','ECG2'})));
EEG = eeg_checkset(EEG);

%% Downsample data:
newsmpl = 128;
if EEG.srate>newsmpl
    NewSamplingRate = newsmpl;
    EEG = pop_resample(EEG, NewSamplingRate);
end

EEGori = EEG; % save data before channel rejection

%% Filter the data from .5 to 45 Hz. First High pass then Low pass the dataset
[EEG com b] = pop_eegfiltnew(EEG,.5,[]);
[EEG com b] = pop_eegfiltnew(EEG,[],45);

pop_eegplot(EEG, 1, 1, 1);

%% DEFINE BAD CHANNELS MANUALLY HERE
table = readtable(file_badChans);
i = find(table.Number == sujnumber);
badChan = table.badChan(i);
allSubs(isub).Badchannels = split(badChan)';
EEG = pop_select(EEG,'nochannel',find(ismember({EEG.chanlocs.labels},allSubs(isub).Badchannels)));

outputfilename = sprintf('%s_noepoch.set',sujfolder);
EEG = pop_saveset( EEG,'filename',outputfilename,'filepath',outfold);


%% rename events properly
trigstart   = 101; % trial onset: goes from 101 to 130: stim displayed
trigstim    = 12; % stim onset : stim starting to move = trigstart + 300;
trigM1      = 13; % M1 mouse first move
trigR1      = 14; % R1 provided
trigQ2      = 15; % Q2 onset
trigM2      = 16; % M2 mouse first move during VAS
trigR2      = 17; % R2 provided

for e = 1:length(EEG.event)
    EEG.event(e).type = floor(double(EEG.event(e).type)/256);
end

%% Check adequacy between trigR1 and RTs 
[cor, P, EEG, screen] = check_RTs(EEG,behavroot,sujnumber);

%% Compute latencies for movement onset (trigger M1)
% Recompute movement onset based on a thresholding of mouse speed (20%)
P = recompute_movonset(P, screen);

%% recreate EEG triggers according to movonset
recreate_trigger;

%% Epoch the data
ev = find([EEG.event.type] == trigM1); 
diflat =  [diff([EEG.event(ev).latency]) 1001]; % compute ITI
goodidx = find(diflat > 500); % remove triggers with too short ITI (sometimes triggers are played twice by mistake)
ev = ev(goodidx);

goodepochs = find([EEG.event(ev).keepM1] == 1);
EEG.rejectedM1 = find([EEG.event(ev).keepM1] == 0);
EEG = pop_epoch(EEG,{},[-1 2], 'epochinfo', 'no', 'eventindices', ev(goodepochs));

%% removing predefined bad trials with no type 1 response
maxrt = 6; % here set the max allowed RT
rt_tmp = [];
for e=1:length(EEG.epoch)
    if length(EEG.epoch(e).eventRT{1}) == 0
        rt_tmp(e) = NaN;
    else rt_tmp(e) = EEG.epoch(e).eventRT{1};
    end
end

EEG.rejectedmaxrt = find(rt_tmp>maxrt);
EEG = pop_select(EEG,'notrial',find(rt_tmp>maxrt));

%% save with raw epoch info
outputfilename = sprintf('%s_all.set',sujfolder);
EEG = pop_saveset( EEG,'filename',outputfilename,'filepath',outfold);
cd(outfold)

%% average referencing (after bad channel rejection)
EEG = pop_reref(  EEG, []); %!
EEG = pop_rmbase( EEG, []); % center all epochs
EEG = eeg_checkset(EEG, 'eventconsistency');

%% VISUAL INSPECTION HERE 
pop_eegplot(EEG, 1, 1,0); % if bad channels detected here, go back to cleaning step while excluding badchannels l.126

%% Reject Marked trials
EEG.rejpreICA = find(EEG.reject.rejmanual);
EEG = pop_select(EEG,'notrial',find(EEG.reject.rejmanual));

EEG = eeg_checkset( EEG );


%% create fake EOG channels
if ismember('AFz', allSubs(isub).Badchannels) || ismember('FPz', allSubs(isub).Badchannels)
    EEG.data(length(EEG.chanlocs)+1,:,:) = EEG.data(find(strcmp({EEG.chanlocs.labels},'AF4')),:,:) - EEG.data(find(strcmp({EEG.chanlocs.labels},'FP2')),:,:); % veog
else
    EEG.data(length(EEG.chanlocs)+1,:,:) = EEG.data(find(strcmp({EEG.chanlocs.labels},'AFz')),:,:) - EEG.data(find(strcmp({EEG.chanlocs.labels},'FPz')),:,:); % veog
end
if ismember('AF8', allSubs(isub).Badchannels) 
	if ismember('AF7', allSubs(isub).Badchannels)
        EEG.data(length(EEG.chanlocs)+2,:,:) = EEG.data(find(strcmp({EEG.chanlocs.labels},'F7')),:,:) - EEG.data(find(strcmp({EEG.chanlocs.labels},'F8')),:,:); % heog
    else
        EEG.data(length(EEG.chanlocs)+2,:,:) = EEG.data(find(strcmp({EEG.chanlocs.labels},'AF7')),:,:) - EEG.data(find(strcmp({EEG.chanlocs.labels},'F8')),:,:); % heog
    end
else
    EEG.data(length(EEG.chanlocs)+2,:,:) = EEG.data(find(strcmp({EEG.chanlocs.labels},'AF7')),:,:) - EEG.data(find(strcmp({EEG.chanlocs.labels},'AF8')),:,:); % heog
end
EEG.chanlocs(length(EEG.chanlocs)+1).labels = 'veog';
EEG.chanlocs(length(EEG.chanlocs)+1).labels = 'heog';

EEG.datatmp = reshape(EEG.data,size(EEG.data,1),size(EEG.data,2)*size(EEG.data,3));
[COEFF,SCORE,latent,tsquare] = princomp(EEG.datatmp');
EEG.num_components_to_keep = find(cumsum(latent) ./ sum(latent)>0.99,1)+1;
EEG = rmfield(EEG,'datatmp');


%% run ICA
if dorunica == 1
    EEG = pop_runica(EEG, 'icatype','runica', 'chanind', 1:length(EEG.chanlocs),'pca', EEG.num_components_to_keep);% 
    EEG = eeg_checkset(EEG, 'ica');
    outputfilename = sprintf('%s_all_ICA.set',sujfolder);
    EEG = pop_saveset( EEG,'filename',outputfilename,'filepath',outfold);
else
    outputfilename = sprintf('%s_all_ICA.set',sujfolder);
    EEG = pop_loadset([outfold '/' outputfilename]);
end

%% use 'Adjust' to automatically remove ica components:
[EEG com] = pop_autorejICA([]);

%% Reject non-neural components
rejects = find(EEG.reject.gcompreject);
EEG = pop_subcomp(EEG,rejects);
EEG.rm_ICs = rejects;

%% remove eog channels here
EEG = pop_select(EEG,'nochannel',[length(EEG.chanlocs)-1 length(EEG.chanlocs)]);

%% VISUAL INSPECTION after ICA
pop_eegplot(EEG, 1, 1,0); % if bad channels detected here, go back to cleaning step while excluding badchannels l.126

%% remove residual artifacts
EEG.rejpostICA = find(EEG.reject.rejmanual);
EEG = pop_select(EEG,'notrial',find(EEG.reject.rejmanual));

EEG = eeg_checkset( EEG );
% pop_eegplot(EEG, 1, 1, 1);

%% Reinterpolate bad channels.
EEG = pop_interp(EEG, EEGori.chanlocs(1:63), 'spherical');

if length(EEG.chanlocs)~=63
    error('EEG.chanlocs does not contain 63 channels after interpolation!')
end


%% create clean epoch for easy R loading

for e = 1:length(EEG.epoch)
    EEG.newepoch(e).cor = EEG.epoch(e).eventcor{1};
    EEG.newepoch(e).rt = EEG.epoch(e).eventRT{1};
    EEG.newepoch(e).trial = EEG.epoch(e).eventtrial{1};
    EEG.newepoch(e).signal = EEG.epoch(e).eventsignal{1};
    EEG.newepoch(e).dir = EEG.epoch(e).eventdir{1};
    EEG.newepoch(e).conf = EEG.epoch(e).eventconf{1};
    EEG.newepoch(e).rt2 = EEG.epoch(e).eventRT2{1};
    EEG.newepoch(e).epoch = e; 
end

EEG.epoch = EEG.newepoch;
EEG = rmfield(EEG,'newepoch');

%% Detect Change of mind
[EEG, ch_mind, rt_chmind] = extract_chmind(EEG,P);
length(find(ch_mind))
plot_chmind;

%% SAVE
outputfilename = sprintf('%s_all_ICA_clean.set',sujfolder);
EEG = pop_saveset(EEG,'filename',outputfilename,'filepath',outfold);
EEG = pop_loadset([outfold '/' outputfilename]);

clear EEG
