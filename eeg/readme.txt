This folder contains the preprocessing and analysis scripts corresponding to the following article: 
Preserved electrophysiological markers of confidence in schizophrenia spectrum disorder
Martin Rouy, Matthieu Roger, Dorian Goueytes, Michael Pereira, Paul Roux, and Nathan Faivre

Article DOI: 10.1038/s41537-023-00333-4

Preprocessing is done under Matlab (eeglab) with preprocessing.m
Preprocessed data can be found here: https://openneuro.org/datasets/ds004368/versions/1.0.2

Statistical analyses of preprocessed data is done under R using analysis/eeg.Rproj

For any request please contact Martin Rouy or Nathan Faivre (firstname.lastname@univ-grenoble-alpes.fr)
