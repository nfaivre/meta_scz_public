## STATS
# run lmer on accuracy only, with group as fixed effect
# parallel computing
cluster <- new_cluster(ncores)
cluster_library(cluster, c('afex','purrr','dplyr'))
gc()

win <- c(.05,.200)
if (do_lmer_confcorrect_byGroup){
  chans=unique(a$chan);
  times = unique(a$time[a$time<=win[2] & a$time>=win[1]]);
  allres_confc = c()
  
  for (c in 1:length(times)) { #
    print(paste('fitting timepoint = ', times[c], 'ms', sep=''))
    allres_confc[[c]] = a %>% 
      filter (time == times[c], 
              cond == "c",
              cor == "Correct", 
              com == 0, 
              beyondChance == 1) %>% 
      dplyr::select(amp_bl, zconf, suj, chan, zrt) %>%
      nest(-chan) %>%
      mutate(lme_out = map(data, ~mixed(amp_bl ~ zconf + zrt +(zconf|suj), data = ., method='S', progress=F))) %>%
      mutate(perf = map(lme_out,"anova_table")) %>%
      mutate(F = map(perf,'F'), p = map(perf,'Pr(>F)')) %>%
      select(-perf,-data,-lme_out) %>% as.data.frame() %>%
      collect() %>% mutate(time =times[c])
  }
  
  ## reorganize statistics 
  allres_confc = rbindlist(allres_confc) # concatenate the list allres
  
  Fs = as_tibble(matrix(unlist(allres_confc$F), ncol=2, byrow = T)); 
  names(Fs) = paste('F', c('main','covar'), sep = '') #
  ps = as_tibble(matrix(unlist(allres_confc$p), ncol=2, byrow = T)); 
  names(ps) = paste('p', c('main','covar'), sep = '') #
  
  stats_c = cbind(allres_confc, Fs, ps) %>% select(-F, -p)
  stats_c = stats_c[order(stats_c$time,stats_c$chan),] 
  stats_c$group = "c"
  
  allres_confp = c()
  for (c in 1:length(times)) { #
    print(paste('fitting timepoint = ', times[c], 'ms', sep=''))
    allres_confp[[c]] = a %>% 
      filter (time == times[c], 
              cond == "p",
              com == 0, 
              cor == "Correct",
              beyondChance == 1) %>% 
      dplyr::select(amp_bl, zconf, suj, chan, zrt) %>%
      nest(-chan) %>%
      mutate(lme_out = map(data, ~mixed(amp_bl ~ zconf + zrt + (zconf|suj), data = ., method='S', progress=F))) %>%
      mutate(perf = map(lme_out,"anova_table")) %>%
      mutate(F = map(perf,'F'), p = map(perf,'Pr(>F)')) %>%
      select(-perf,-data,-lme_out) %>% as.data.frame() %>%
      collect() %>% mutate(time =times[c])
  }
  
  ## reorganize statistics 
  allres_confp = rbindlist(allres_confp) # concatenate the list allres
  
  Fs = as_tibble(matrix(unlist(allres_confp$F), ncol=2, byrow = T)); 
  names(Fs) = paste('F', c('main','covar'), sep = '') #
  ps = as_tibble(matrix(unlist(allres_confp$p), ncol=2, byrow = T)); 
  names(ps) = paste('p', c('main','covar'), sep = '') #
  
  stats_p = cbind(allres_confp, Fs, ps) %>% select(-F, -p)
  stats_p = stats_p[order(stats_p$time,stats_p$chan),] 
  stats_p$group = "p"
  
  stats = rbind(stats_c, stats_p)
  stats = stats %>% group_by(group,chan) %>%
    mutate(psfdr = p.adjust (pmain, method='fdr')) # add fdr correction to stats_c
  
  electrodes = stats %>% filter(psfdr < 0.01) %>% ungroup %>% 
    select(chan) %>% unique %>% unlist
  
  p = stats %>% filter(chan %in% chans) %>%
    ggplot(aes(x=time,y=log(psfdr),color=group)) + geom_line() +
    geom_hline(yintercept = c(log(0.05),log(0.01),log(0.001)),linetype = 'dashed',alpha=0.5)+
    geom_vline(xintercept = 0,alpha=.2)+
    scale_y_continuous(breaks = c(0, -1 , log(0.05), log(0.01), log(0.001)),
                       labels = c("0", "-1", "log(0.05)", "log(0.01)", "log(0.001)"))+
    facet_wrap(~chan) + labs(subtitle ='log(p) vs. time per channel')+
    theme(legend.position = "bottom")+
    coord_cartesian(xlim = win)
  
  save(stats, p, file=  paste(path_models,'lmer_zconfcorrect_cov_byGroup.RData', sep='/'))
} 
