
cluster <- new_cluster(ncores)
cluster_library(cluster, c('afex','purrr','dplyr'))
gc()

if (do_bayesian_type1_cluster){
  analysis = "type1"
  source(here::here('functions','load_analysis.R'))
  if(effect == "main"){
    chans = electrodes_main
  } else {
    chans = electrodes_inter
  }
  analysis = "type1_cluster_bayesian"
  times = unique(a$time[a$time<=rangeEffect$maxTime & a$time>=rangeEffect$minTime]);
  sequential = FALSE

  sd = 1
  priors <- c(set_prior(paste0("normal(1,",sd,")"), class = "b", coef = "corCorrect"),
              set_prior(paste0("normal(0,",sd,")"), class = "b", coef = "condp"),
              set_prior(paste0("normal(-1,",sd,")"), class = "b", coef = "corCorrect:condp"))
  
  h = c("cor" = "corCorrect > 0",
        "group" = "condp = 0",
        "cor * group" = "corCorrect:condp = 0")

  bayes_cor = a %>% 
    filter (time %in% times,
            chan %in% chans,
            com == 0) %>% group_by(cond,suj,chan,trial,cor) %>% 
    summarise(zamp_avg = mean(zamp, na.rm = TRUE)) %>% 
    brm(zamp_avg ~ cor * cond + (cor|suj/chan), 
        data = .,
        family = "gaussian",
        prior = priors,
        sample_prior = T,
        chains = chains, iter = iter, warmup = warmup,
        cores = chains, save_all_pars = TRUE,
        file = paste(path_models,paste0("glm_cor-cond_rslopes_sd",sd),sep = '/'))
  
  post <- posterior_samples(bayes_cor, pars = "^b_")
  
  (hyp = hypothesis(bayes_cor,h))
  BFmain = round(hyp$hypothesis$Evid.Ratio[hyp$hypothesis$Hypothesis == "cor"],2)
  BFinter= round(hyp$hypothesis$Evid.Ratio[hyp$hypothesis$Hypothesis == "cor*cond"],2)
  
  sprintf("BF for main effect of correctness: %.3f", BFmain)
  sprintf("BF for interaction effect of correctness: %.3f", BFinter)
  
  save(BFmain,BFinter, file= paste(path_models, paste0('brm_corxcond_cluster_BFs_sd',sd,'.RData'), sep = '/'))
}
 